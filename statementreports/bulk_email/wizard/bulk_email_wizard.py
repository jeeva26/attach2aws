# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
import json

class BulkEmailWizard(models.TransientModel):
    _name="bulk.email.wizard"
    
    num_items_selected = fields.Integer(string="Number of Emails", readonly=True)
    template = fields.Char()
    model = fields.Char()
    active_ids = fields.Char()
    
    @api.model
    def default_get(self, fieldnames):
        result = super(BulkEmailWizard, self).default_get(fieldnames)
        result['template'] = self.env.context.get("template_reference")
        result['model'] = self.env.context.get("model")
        _ids = self.env.context.get("active_ids",[])
        result['active_ids'] = json.dumps(_ids)
        result['num_items_selected'] = len(_ids)
        return result
    
    @api.multi
    def do_bulk_email(self):
        for r in self:
            _template = self.env.ref(r.template, False)
            items_to_send = self.env[r.model].browse(json.loads(r.active_ids))
            for i in items_to_send:
                _template.send_mail(i.id, force_send=True)
        return {'type': 'ir.actions.act_window_close'}
