# -*- coding: utf-8 -*-

{
    "name": "Bulk Emails",
    "version": "1.0",
    "depends": [
                "base",
                ],
    "author": "Pragmatic Techoft Pvt. Ltd.",
    "category": "Mail",
    "description": """
Send Bulk Emails
================

""",
    'data': ["wizard/bulk_email_view.xml",
             "act_windows.xml",
             ],
    "demo": [],
    "test": [],
    "installable": True,
    "active": True,
}
