# -*- coding: utf-8 -*-

from openerp import fields, models, _, api
from openerp.exceptions import Warning
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

from datetime import datetime
from dateutil.relativedelta import relativedelta

class generate_statements(models.TransientModel):
    _name = "account.partner.statement.wizard"
    _description = "Statement Wizard"

    description = fields.Char(string='Description')
    title = fields.Char(string='Title (for printing)')
    config_id = fields.Many2one('account.partner.statement.config', string='Type', required=True)
    statement_from = fields.Date(string='Statement From', required=True, help='All reconciled transactions from this date will be included.')
    statement_date = fields.Date(string='Statement Date', required=True)
    ignore_overlap = fields.Boolean(string='Ignore Overlap', help="By default, a partner which already has a statement overlapping with the statement period will be ignored.  This allows those partners to generate an overlapping statement.")
    partner_ids = fields.Many2many('res.partner', domain=[('customer', '=', True)], string='Filter on partner', help="Only selected partners will be printed. Leave empty to include all partners.")

    @api.multi
    def do_generation(self):
        if self.statement_date >= fields.Date.context_today(self):
            raise Warning(_('Statements cannot be for future.'))

        if self.statement_from > self.statement_date:
            raise Warning(_('Statement Date must be after Statement From.'))

        # Partner goes to config they are linked to, or if none, to config their payment terms go to, or if none, to "default"
        self.env.cr.execute("""
WITH ptr AS (SELECT account_payment_term_id apt_id, min(account_partner_statement_config_id) statement_config_id
    FROM account_partner_statement_config_account_payment_term_rel
    GROUP BY account_payment_term_id
    )

SELECT id, statement_config_id
FROM res_partner
WHERE statement_config_id IS NOT NULL
UNION
SELECT part.id, ptr.statement_config_id
FROM res_partner part,
    account_payment_term pt,
    ptr,
    ir_property prop
WHERE part.statement_config_id IS NULL
    AND prop.name='property_payment_term_id'
    AND prop.value_reference=('account.payment.term,' || pt.id::TEXT)
    AND prop.res_id=('res.partner,' || part.id::TEXT)
    AND ptr.apt_id=pt.id
    AND ptr.statement_config_id IS NOT NULL
        """,)

        explicit_partners = {}
        partners_with_links = []
        for row in self.env.cr.fetchall():
            explicit_partners[row[0]] = row[1]
            partners_with_links.append(row[0])
        print"\n\n\n\n partners_with_links--------------------",partners_with_links
        if self.partner_ids:
            partner_ids = [p.id for p in self.partner_ids if (explicit_partners.get(p.id, 0) == self.config_id.id) or (self.config_id.default and not p.id in explicit_partners)]
        else:
            partner_ids = [ep[0] for ep in explicit_partners.iteritems() if ep[1] == self.config_id.id]
            if self.config_id.default:
                partner_ids.extend(self.env['res.partner'].search([('id', 'not in', partners_with_links)]).ids)
        print" 0000000000000000000 doo generation wizard------------------"
        if partner_ids:
            batch = self.env['account.partner.statement.batch'].create_batch(self.config_id.id,
                                                                             'generated',
                                                                             {'description': self.description,
                                                                              'title': self.title,
                                                                              },
                                                                              self.statement_from,
                                                                              self.statement_date,
                                                                              partner_ids,
                                                                              ignore_overlap = self.ignore_overlap
                                                                              )
            print" tax_reportin doo generation wizard------------------"
            return batch.view_batch_statements()
        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    @api.onchange('config_id')
    def onchange_config(self):
        if self.config_id:
            last_batch = self.env['account.partner.statement.batch'].search([('type', '=', 'generated'), ('config_id', '=', self.config_id.id)], order='statement_date desc', limit=1)
            if last_batch:
                statement_from, statement_date = self.config_id.next_date(datetime.strptime(last_batch.statement_date, DEFAULT_SERVER_DATE_FORMAT))
            else:
                statement_from, statement_date = self.config_id.first_date()
            self.statement_from = statement_from.strftime(DEFAULT_SERVER_DATE_FORMAT)
            self.statement_date = statement_date.strftime(DEFAULT_SERVER_DATE_FORMAT)
            self.generate_name_and_title()

    @api.multi
    @api.onchange('statement_from')
    def onchange_date_from(self):
        result = {}
        if self.config_id and self.statement_from:
            discard, to_date = self.config_id.next_date(datetime.strptime(self.statement_from, DEFAULT_SERVER_DATE_FORMAT) - relativedelta(days=1))
            if datetime.strptime(self.statement_date, DEFAULT_SERVER_DATE_FORMAT) != to_date:
                self.statement_date = to_date   # check_dates will be called by the on_change to statement_date...
                self.generate_name_and_title()
            else:
                result = self.check_overlaps()
        return result

    @api.multi
    @api.onchange('statement_date')
    def onchange_date_to(self):
        result = {}
        if self.config_id and self.statement_from and self.statement_date:
            self.generate_name_and_title()
            result = self.check_overlaps()
        return result

    def check_overlaps(self):
        result = {}
        last_batch = self.env['account.partner.statement.batch'].search([('type', '=', 'generated'), ('config_id', '=', self.config_id.id)], order='statement_date desc', limit=1)

        statement_from = datetime.strptime(self.statement_from, DEFAULT_SERVER_DATE_FORMAT)
        statement_date = datetime.strptime(self.statement_date, DEFAULT_SERVER_DATE_FORMAT)

        warnings = []
        if self.config_id.calc_type == 'monthly' and statement_date != statement_date + relativedelta(day=self.config_id.day_of_month): # this works nicely for EOM type settings, too
            warnings.append('Statement date does not match configuration.')
        if self.config_id.calc_type == 'weekly' and statement_date.weekday() != int(self.config_id.day_of_week):
            warnings.append('Statement date does not match configuration.')
        if last_batch and statement_from <= datetime.strptime(last_batch.statement_date, DEFAULT_SERVER_DATE_FORMAT):
            warnings.append('Statement is not after last statement.')
        if last_batch and statement_from > datetime.strptime(last_batch.statement_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(days=1):
            warnings.append('Statement misses days after last statement.')
        if statement_date < statement_from:
            warnings.append('Statement start is after statement date.')
        if warnings:
            result['warning'] = {'title': 'Date Warnings', 'message': '\n\n'.join(warnings)}
        return result

    def generate_name_and_title(self):
        if self.config_id and self.statement_from and self.statement_date:
            self.title = 'Statement for %s' % datetime.strptime(self.statement_date, DEFAULT_SERVER_DATE_FORMAT).strftime('%e %b %Y')
            overlap_count = len(self.env['account.partner.statement.batch'].search([('type', '=', 'generated'), ('config_id', '=', self.config_id.id),('statement_date', '>=', self.statement_from),('statement_from', '<=', self.statement_date)]))
            self.description = '%s on %s%s' % (self.config_id.name, self.statement_date, overlap_count and ' (%s)' % (overlap_count + 1) or '')
