# -*- coding: utf-8 -*-

{
    "name": "Account Partner Statements",
    "version": "1.0",
    "depends": ["base","account","report"
               ],
    "author": "Pragmatic Techsoft Pvt. Ltd.",
    "category": "Accounting & Finance",
    "website": "www.pragtech.co.in",
    "description": """
Account Partner Statements
==========================

This module defines an intermediary table which stores partner statements.
Statements are generated at a point in time, and are kept as a batch.  From
that batch, statements can be printed, or reprinted.
""",
    'data': [ 
             "security/ir.model.access.csv",
             'report/ir.actions.report.xml',
             'report/statement_report.xml',
             'report/activity_statement_report.xml',
             "data/partner_statement_data.xml",
             "views/partner_statement_view.xml",
             'wizard/statement_generate_wizard.xml',
             'wizard/statement_adhoc_wizard.xml',
            ],
    "demo": [],
    "test": [],
    "installable": True,
    "active": True,
}
