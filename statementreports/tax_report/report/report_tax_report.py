from odoo import models, api, fields, _, SUPERUSER_ID
from openerp.tools.misc import formatLang
import time,datetime
from openerp.report import report_sxw
from datetime import date,datetime

  
class report_tax_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(report_tax_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'get_lines': self.get_lines,
            'get_data': self.get_data,
            'get_purchase_lines':self.get_purchase_lines,
            'get_detail_lines':self.get_detail_lines,
            'get_detail_purchase_lines':self.get_detail_purchase_lines,
            'get_sale_total':self.get_sale_total,
            'get_purchase_total':self.get_purchase_total,
            'get_date':self.get_date
            
        })
        
    def get_lines(self,data):
        lst =[]
        env = api.Environment(self.cr, SUPERUSER_ID, {})
        self.cr.execute('select invoice_id from account_invoice_tax')
        acc_tax = self.cr.fetchall()
        acc_tax_inv = []
        if acc_tax:
            acc_tax_inv=[i[0] for i in acc_tax]
        self.cr.execute("select id from account_move where state = 'posted'")
        acc_mov = self.cr.fetchall()
        move = env['account.move'].search([])
        if acc_mov:
            acc_moves=[i[0] for i in acc_mov]
        if acc_tax_inv:
            
            if data["form"]["date_from"] and data["form"]["date_to"]:
                end_date = data["form"]["date_to"]
                start_date = data["form"]["date_from"]
                
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        sale = env['account.invoice'].search([('date_invoice','<=',end_date),('date_invoice','>=',start_date),('id','in',acc_tax_inv),('move_id','in',acc_moves)])
                else:
                    sale = env['account.invoice'].search([('date_invoice','<=',end_date),('date_invoice','>=',start_date),('id','in',acc_tax_inv),('move_id','in',move.mapped('id'))])
            else:
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        sale = env['account.invoice'].search([('id','in',acc_tax_inv),('move_id','in',acc_moves)])
                else:
                    sale = env['account.invoice'].search([('id','in',acc_tax_inv),('move_id','in',move.mapped('id'))])
            lst =[]
            taxes = env['account.tax'].search([('type_tax_use','=','sale')])
            tax_tot=0
            for t in taxes:
                tax_tot=0
                res = {}
                if sale:
                    self.cr.execute('select child_tax from account_tax_filiation_rel where parent_tax = %s',(t.id,))
                    child = self.cr.fetchall()
                    if child:
                        child_taxes=[i[0] for i in child]
                    else:
                        child_taxes=[t.id]
                    self.cr.execute('select sum(amount) from account_invoice_tax where tax_id in %s AND invoice_id in  %s',(tuple(child_taxes),tuple(sale.mapped('id')),))
                    tamt = self.cr.fetchall()
                    if tamt:
                        tax_tot=tamt[0][0]
                        total=0
                        subtot_amt = 0
                        self.cr.execute("select sum(amount_untaxed) as subtotal ,sum(amount_total) as total from account_invoice where move_id in (select id from account_move) and id in (select invoice_id from account_invoice_tax) and id in (select invoice_id from account_invoice_line where id in (select invoice_line_id from account_invoice_line_tax where tax_id =%s))",(t.id,))
                        amts_dict = self.cr.dictfetchall()[0]
#                         tax_brw=env['account.tax'].browse(t)
                        if tax_tot:
                            res={'tax':float(tax_tot),'total':amts_dict['total'],'subtot_amt':amts_dict['subtotal'],'tname':t.name}
                            lst.append(res)
        return lst
    
    def get_purchase_lines(self,data):
        lst =[]
        env = api.Environment(self.cr, SUPERUSER_ID, {})
        self.cr.execute('select invoice_id from account_invoice_tax')
        acc_tax = self.cr.fetchall()
        if acc_tax:
            acc_tax_inv=[i[0] for i in acc_tax]
        self.cr.execute("select id from account_move where state = 'posted'")
        acc_mov = self.cr.fetchall()
        move = env['account.move'].search([])
        if acc_mov:
            acc_moves=[i[0] for i in acc_mov]
        if acc_tax_inv:
            if data["form"]["date_from"] and data["form"]["date_to"]:
                end_date = data["form"]["date_to"]
                start_date = data["form"]["date_from"]
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        purchase = env['account.invoice'].search([('date_invoice','<=',end_date),('date_invoice','>=',start_date),('id','in',acc_tax_inv),('move_id','in',acc_moves)])
                else:
                    purchase = env['account.invoice'].search([('date_invoice','<=',end_date),('date_invoice','>=',start_date),('id','in',acc_tax_inv),('move_id','in',move.mapped('id'))])
            
            else:
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        purchase = env['account.invoice'].search([('id','in',acc_tax_inv),('move_id','in',acc_moves)])
                else:
                    purchase = env['account.invoice'].search([('id','in',acc_tax_inv),('move_id','in',move.mapped('id'))])
            taxes = env['account.tax'].search([('type_tax_use','=','purchase')])
            lst =[]
            for t in taxes:
                res={}
                if purchase:
                    self.cr.execute('select child_tax from account_tax_filiation_rel where parent_tax = %s',(t.id,))
                    child = self.cr.fetchall()
                    if child:
                        child_taxes=[i[0] for i in child]
                    else:
                        child_taxes=[t.id]
                    self.cr.execute('select sum(amount) from account_invoice_tax where tax_id in %s AND invoice_id in  %s',(tuple(child_taxes),tuple(purchase.mapped('id')),))
                    tamt = self.cr.fetchall()
                    if tamt:
                        tax_tot=tamt[0][0]
                    
                    total=0
                    subtot_amt = 0
                    self.cr.execute("select sum(amount_untaxed) as subtotal ,sum(amount_total) as total from account_invoice where move_id in (select id from account_move) and id in (select invoice_id from account_invoice_tax) and id in (select invoice_id from account_invoice_line where id in (select invoice_line_id from account_invoice_line_tax where tax_id =%s))",(t.id,))
                    amts_dict = self.cr.dictfetchall()[0]
#                     tax_brw=env['account.tax'].browse(t)
                    if tax_tot:
                        res={'tax':float(tax_tot),'total':amts_dict['total'],'subtot_amt':amts_dict['subtotal'],'tname':t.name}
                        lst.append(res)
        return lst
     
    
    def get_data(self, data):
        result ={'display_acc':'','date_from':'','date_to':'','target':'','report_type':''}
        if data['form']['display_account']:
            result.update({'display_acc':data['form']['display_account']})
        if data['form']['date_from']:
            result.update({ 'date_from':data['form']['date_from']}) 
        if data['form']['date_to']:
            result.update({'date_to': data['form']['date_to']}) 
        if data['form']['target_move']:
            result.update({'target':data['form']['target_move']})
        if data['form']['report_type']:
            result.update({'report_type':data['form']['report_type']})    
        return result
        
    def get_date(self, data):
        res={}
        env = api.Environment(self.cr, SUPERUSER_ID, {})
        user =env['res.users'].browse(self.uid)
        lang = env['res.lang'].search([('code','=',user.partner_id.lang)])
        if lang:
#             lang_brw=env['res.lang'].browse(lang[0])
            if data['form']['date_from']:
                res['frm_date']=datetime.strptime(data['form']['date_from'],"%Y-%m-%d").strftime(lang.date_format)
            if data['form']['date_to']:
                res['to_date']=datetime.strptime(data['form']['date_to'],"%Y-%m-%d").strftime(lang.date_format)
        else:
            if data['form']['date_from']:
                frm_date=data['form']['date_from']
                res['frm_date']=frm_date
            if  data['form']['date_to']:  
                to_date=data['form']['date_to']
                res['to_date']=to_date
        return res
    
    def get_detail_lines(self,data):
        env = api.Environment(self.cr, SUPERUSER_ID, {})
        self.cr.execute('select invoice_id from account_invoice_tax')
        acc_tax = self.cr.fetchall()
        acc_tax_inv = []
        if acc_tax:
            acc_tax_inv=[i[0] for i in acc_tax]
        taxes = env['account.tax'].search([('type_tax_use','=','sale')])
        inv_lst=[]
        self.cr.execute("select id from account_move where state = 'posted'")
        acc_mov = self.cr.fetchall()
        move = env['account.move'].search([])
        if acc_mov:
            acc_moves=[i[0] for i in acc_mov]
        if  acc_tax_inv:   
            if data["form"]["date_from"] and data["form"]["date_to"]:
                end_date = data["form"]["date_to"]
                start_date = data["form"]["date_from"]
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        self.cr.execute("SELECT id from account_invoice  where date_invoice <= %s AND date_invoice >= %s AND id in %s AND move_id in %s",( end_date,start_date,tuple(acc_tax_inv),tuple(acc_moves),))
                        inv_res = self.cr.fetchall()
                else:
                    self.cr.execute("SELECT id from account_invoice  where  date_invoice <= %s AND date_invoice >= %s AND id in %s AND move_id in %s",( end_date,start_date,tuple(acc_tax_inv),tuple(move.mapped('id')),))
                    inv_res = self.cr.fetchall()
            else:
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        self.cr.execute("SELECT id from account_invoice  where  id in %s AND move_id in %s",(tuple(acc_tax_inv),tuple(acc_moves),))
                        inv_res = self.cr.fetchall()
                else:
                    self.cr.execute("SELECT id from account_invoice  where  id in %s AND move_id in %s",(tuple(acc_tax_inv),tuple(move.mapped('id')),))
                    inv_res = self.cr.fetchall()
            
            
            inv_lst=[i[0] for i in inv_res]
            tx_amt=0
            
        l=[]
        for tax in taxes:
            tot_amt=0
            result={}
#             tax_brw=env ['account.tax'].browse(tax)
            lst =[]
            tx_amt=0
            
            if inv_lst:
                self.cr.execute('select child_tax from account_tax_filiation_rel where parent_tax = %s',(tax.id,))
                child = self.cr.fetchall()
                if child:
                    child_taxes=[i[0] for i in child]
                else:
                    child_taxes=[tax.id]
                for mv in inv_lst:
                    tax_tot=0
                    self.cr.execute('select sum(amount) from account_invoice_tax where tax_id in %s AND invoice_id =  %s',(tuple(child_taxes),mv,))
                    tamt = self.cr.fetchall()
                    inv_brw=env['account.invoice'].browse(mv)
                    
                    if tamt:
                        tax_tot=tamt[0][0]
                        tot_amt=tot_amt+inv_brw.amount_untaxed
                        lang = env['res.lang'].search([('code','=',inv_brw.partner_id.lang)])
                        if lang:
#                             lang_brw=env['res.lang'].browse(lang[0])
                            inv_date=datetime.strptime(inv_brw.date_invoice,"%Y-%m-%d").strftime(lang.date_format)
                        else:
                            inv_date=inv_brw.date_invoice
                        self.cr.execute("select sum(amount_untaxed) as subtotal  from account_invoice where move_id in (select id from account_move) and id in (select invoice_id from account_invoice_tax) and id in (select invoice_id from account_invoice_line where id in (select invoice_line_id from account_invoice_line_tax where tax_id =%s))",(tax.id,))
                        amts_dict = self.cr.dictfetchall()[0]
                    
                        if tax_tot:
                            tx_amt= tx_amt +tax_tot
                            lst.append({'inv_name':inv_brw.number,'tax':tax_tot,'tot':inv_brw.amount_untaxed,'partner':inv_brw.partner_id.name,'date':inv_date,'ref':inv_brw.origin})
                            
            if lst:
                result={'tx_amt': tx_amt,'inv':lst ,'tax_name':tax.name,'tot_amt':amts_dict['subtotal']}
            if result:
                l.append(result)
        return l
    
    def get_detail_purchase_lines(self,data):
        env = api.Environment(self.cr, SUPERUSER_ID, {})
        self.cr.execute('select invoice_id from account_invoice_tax')
        acc_tax = self.cr.fetchall()
        acc_tax_inv = []
        if acc_tax:
            acc_tax_inv=[i[0] for i in acc_tax]
        taxes = env['account.tax'].search([('type_tax_use','=','purchase')])
        inv_lst=[]
        self.cr.execute("select id from account_move where state = 'posted'")
        acc_mov = self.cr.fetchall()
        if acc_mov:
            acc_moves=[i[0] for i in acc_mov]
        l=[]
        move = env['account.move'].search([])
        if acc_tax_inv:
            if data["form"]["date_from"] and data["form"]["date_to"]:
                end_date = data["form"]["date_to"]
                start_date = data["form"]["date_from"]
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        self.cr.execute("SELECT id from account_invoice  where   date_invoice <= %s AND date_invoice >= %s AND id in %s AND move_id in %s",( end_date,start_date,tuple(acc_tax_inv),tuple(acc_moves),))
                        inv_res = self.cr.fetchall()
                else:
                    self.cr.execute("SELECT id from account_invoice  where  date_invoice <= %s AND date_invoice >= %s AND id in %s AND move_id in %s",( end_date,start_date,tuple(acc_tax_inv),tuple(move.mapped('id')),))
                    inv_res = self.cr.fetchall()
            else:
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        self.cr.execute("SELECT id from account_invoice  where   id in %s AND move_id in %s",(tuple(acc_tax_inv),tuple(acc_moves),))
                        inv_res = self.cr.fetchall()
                else:
                    self.cr.execute("SELECT id from account_invoice  where id in %s AND move_id in %s",(tuple(acc_tax_inv),tuple(move.mapped('id')),))
                    inv_res = self.cr.fetchall()
            inv_lst=[i[0] for i in inv_res]
            
            for tax in taxes:
                tx_amt=0
                tot_amt=0
                result={}
#                 tax_brw=env['account.tax'].browse(tax)
                lst =[]
                tx_amt=0
                if inv_lst:
                    self.cr.execute('select child_tax from account_tax_filiation_rel where parent_tax = %s',(tax.id,))
                    child = self.cr.fetchall()
                    if child:
                        child_taxes=[i[0] for i in child]
                    else:
                        child_taxes=[tax.id]
                    for mv in inv_lst:
                        self.cr.execute('select sum(amount) from account_invoice_tax where tax_id in %s AND invoice_id =  %s',(tuple(child_taxes),mv,))
                        tamt = self.cr.fetchall()
                        inv_brw=env['account.invoice'].browse(mv)
                        tot_amt=tot_amt+inv_brw.amount_untaxed
                        if tamt:
                            tax_tot=tamt[0][0]
                            lang = env['res.lang'].search([('code','=',inv_brw.partner_id.lang)])
                            if lang:
#                                 lang_brw=env['res.lang'].browse(lang[0])
                                inv_date=datetime.strptime(inv_brw.date_invoice,"%Y-%m-%d").strftime(lang.date_format)
                            if tax_tot:
                                lst.append({'inv_name':inv_brw.number,'tax':tax_tot,'tot':inv_brw.amount_untaxed,'partner':inv_brw.partner_id.name,'date':inv_date,'ref':inv_brw.origin})
                                tx_amt= tx_amt +tax_tot
                    self.cr.execute("select sum(amount_untaxed) as subtotal  from account_invoice where move_id in (select id from account_move) and id in (select invoice_id from account_invoice_tax) and id in (select invoice_id from account_invoice_line where id in (select invoice_line_id from account_invoice_line_tax where tax_id =%s))",(tax.id,))
                    amts_dict = self.cr.dictfetchall()[0]
                    
                                   
                if lst:
                    result={'tx_amt': tx_amt,'inv':lst ,'tax_name':tax.name,'tot_amt':amts_dict['subtotal']}
                if result:
                    l.append(result)
        return l
    
    def get_sale_total(self,data):
        res={}
        sale_lines = self.get_lines(data)
        tot=0
        for sal in sale_lines:
            tot_tax = tot+sal['tax']
            tot = tot+sal['total']
            sub = tot+sal['subtot_amt']
        res = {'tot_tax':tot_tax,'tot':tot,'sub':sub}
        return res    
    
    def get_purchase_total(self,data):
        res={}
        sale_lines = self.get_purchase_lines(data)
        tot=0
        for sal in sale_lines:
            tot_tax = tot+sal['tx_amt']
            tot = tot+sal['tot_amt']
            sub = tot+sal['subtot_amt']
        res = {'tot_tax':tot_tax,'tot':tot,'sub':sub}
        return res    
    
    
class report_tax_document_new(models.AbstractModel):
    _name = 'report.tax_report.report_tax'
    _inherit = 'report.abstract_report'
    _template = 'tax_report.report_tax'
    _wrapped_report_class = report_tax_report

