from odoo import models, api, fields, _, SUPERUSER_ID
from openerp.tools.misc import formatLang
import time,datetime
from openerp.report import report_sxw
from datetime import date,datetime
  
class ReportTaxReport(report_sxw.rml_parse):
    
    @api.model_cr
    def __init__(self):
        super(ReportTaxReport, self).__init__()
        self.localcontext.update({
            'time': time,
            'get_lines': self.get_lines,
            'get_data': self.get_data,
            'get_purchase_lines':self.get_purchase_lines,
            'get_detail_lines':self.get_detail_lines,
            'get_detail_purchase_lines':self.get_detail_purchase_lines,
            'get_sale_total':self.get_sale_total,
            'get_purchase_total':self.get_purchase_total,
            'get_date':self.get_date
            
        })
        
    @api.multi
    def get_lines(self,data):
        print'multiiiiiiiiiii',self,data
        lst =[]
        self.cr.execute('select invoice_id from account_invoice_tax')
        acc_tax = self.cr.fetchall()
        if acc_tax:
            acc_tax_inv=[i[0] for i in acc_tax]
        self.cr.execute("select id from account_move where state = 'posted'")
        acc_mov = self.cr.fetchall()
        move = self.pool.get('account.move').search(self.cr,self.uid,[])
        if acc_mov:
            acc_moves=[i[0] for i in acc_mov]
        if acc_tax_inv:
            
            if data["form"]["date_from"] and data["form"]["date_to"]:
                end_date = data["form"]["date_to"]
                start_date = data["form"]["date_from"]
                
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        sale = self.pool.get('account.invoice').search(self.cr,self.uid,[('date_invoice','<=',end_date),('date_invoice','>=',start_date),('id','in',acc_tax_inv),('move_id','in',acc_moves)])
                else:
                    sale = self.pool.get('account.invoice').search(self.cr,self.uid,[('date_invoice','<=',end_date),('date_invoice','>=',start_date),('id','in',acc_tax_inv),('move_id','in',move)])
            else:
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        sale = self.pool.get('account.invoice').search(self.cr,self.uid,[('id','in',acc_tax_inv),('move_id','in',acc_moves)])
                else:
                    sale = self.pool.get('account.invoice').search(self.cr,self.uid,[('id','in',acc_tax_inv),('move_id','in',move)])
            lst =[]
            taxes = self.pool.get('account.tax').search(self.cr,self.uid,[('type_tax_use','=','sale')])
            tax_tot=0
            print "sale =================================",sale,len(sale)
            for t in taxes:
                tax_tot=0
                res = {}
                if sale:
                    self.cr.execute('select child_tax from account_tax_filiation_rel where parent_tax = %s',(t,))
                    child = self.cr.fetchall()
                    if child:
                        child_taxes=[i[0] for i in child]
                    else:
                        child_taxes=[t]
                    self.cr.execute('select sum(amount) from account_invoice_tax where tax_id in %s AND invoice_id in  %s',(tuple(child_taxes),tuple(sale),))
                    tamt = self.cr.fetchall()
                    if tamt:
                        tax_tot=tamt[0][0]
                        total=0
                        subtot_amt = 0
                        self.cr.execute("select sum(amount_untaxed) as subtotal ,sum(amount_total) as total from account_invoice where move_id in (select id from account_move) and id in (select invoice_id from account_invoice_tax) and id in (select invoice_id from account_invoice_line where id in (select invoice_line_id from account_invoice_line_tax where tax_id =%s))",(t,))
                        amts_dict = self.cr.dictfetchall()[0]
                        tax_brw=self.pool.get('account.tax').browse(self.cr,self.uid,t)
                        print "@@@@@@@@@@@@@@@============",tax_tot,amts_dict,tax_brw
                        if tax_tot:
                            res={'tax':float(tax_tot),'total':amts_dict['total'],'subtot_amt':amts_dict['subtotal'],'tname':tax_brw.name}
                            lst.append(res)
        return lst
    
    @api.multi
    def get_purchase_lines(self,data):
        print'multi222222222222222222222222',self,data
        lst =[]
        self.cr.execute('select invoice_id from account_invoice_tax')
        acc_tax = self.cr.fetchall()
        if acc_tax:
            acc_tax_inv=[i[0] for i in acc_tax]
        self.cr.execute("select id from account_move where state = 'posted'")
        acc_mov = self.cr.fetchall()
        move = self.pool.get('account.move').search(self.cr,self.uid,[])
        if acc_mov:
            acc_moves=[i[0] for i in acc_mov]
        if acc_tax_inv:
            if data["form"]["date_from"] and data["form"]["date_to"]:
                end_date = data["form"]["date_to"]
                start_date = data["form"]["date_from"]
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        purchase = self.pool.get('account.invoice').search(self.cr,self.uid,[('date_invoice','<=',end_date),('date_invoice','>=',start_date),('id','in',acc_tax_inv),('move_id','in',acc_moves)])
                else:
                    purchase = self.pool.get('account.invoice').search(self.cr,self.uid,[('date_invoice','<=',end_date),('date_invoice','>=',start_date),('id','in',acc_tax_inv),('move_id','in',move)])
            
            else:
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        purchase = self.pool.get('account.invoice').search(self.cr,self.uid,[('id','in',acc_tax_inv),('move_id','in',acc_moves)])
                else:
                    purchase = self.pool.get('account.invoice').search(self.cr,self.uid,[('id','in',acc_tax_inv),('move_id','in',move)])
            taxes = self.pool.get('account.tax').search(self.cr,self.uid,[('type_tax_use','=','purchase')])
            lst =[]
            for t in taxes:
                res={}
                if purchase:
                    self.cr.execute('select child_tax from account_tax_filiation_rel where parent_tax = %s',(t,))
                    child = self.cr.fetchall()
                    if child:
                        child_taxes=[i[0] for i in child]
                    else:
                        child_taxes=[t]
                    self.cr.execute('select sum(amount) from account_invoice_tax where tax_id in %s AND invoice_id in  %s',(tuple(child_taxes),tuple(purchase),))
                    tamt = self.cr.fetchall()
                    if tamt:
                        tax_tot=tamt[0][0]
                    
                    total=0
                    subtot_amt = 0
                    self.cr.execute("select sum(amount_untaxed) as subtotal ,sum(amount_total) as total from account_invoice where move_id in (select id from account_move) and id in (select invoice_id from account_invoice_tax) and id in (select invoice_id from account_invoice_line where id in (select invoice_line_id from account_invoice_line_tax where tax_id =%s))",(t,))
                    amts_dict = self.cr.dictfetchall()[0]
                    tax_brw=self.pool.get('account.tax').browse(self.cr,self.uid,t)
                    if tax_tot:
                        res={'tax':float(tax_tot),'total':amts_dict['total'],'subtot_amt':amts_dict['subtotal'],'tname':tax_brw.name}
                        lst.append(res)
        return lst
     
    @api.multi
    def get_data(self, data):
        print'multiiiiiiiiiiiiiiiiiiiiiiii2333333333333333333333',self,data
        result ={'display_acc':'','date_from':'','date_to':'','target':'','report_type':''}
        if data['form']['display_account']:
            result.update({'display_acc':data['form']['display_account']})
        if data['form']['date_from']:
            result.update({ 'date_from':data['form']['date_from']}) 
        if data['form']['date_to']:
            result.update({'date_to': data['form']['date_to']}) 
        if data['form']['target_move']:
            result.update({'target':data['form']['target_move']})
        if data['form']['report_type']:
            result.update({'report_type':data['form']['report_type']})    
        return result
    
    @api.multi    
    def get_date(self, data):
        print'multi44444444444444444444444444444444',self,data
        env = api.Environment(self._cr, SUPERUSER_ID, {})
        res={}
        user = env['res.users'].browse(self.uid)
        lang = env['res.lang'].search([('code','=',user.partner_id.lang)])
        print "llllllllllllllllllllllllllllllllllllll",lang
        if lang:
            lang_brw=self.pool.get('res.lang').browse(self.cr,self.uid,lang[0])
            if data['form']['date_from']:
                res['frm_date']=datetime.strptime(data['form']['date_from'],"%Y-%m-%d").strftime(lang_brw.date_format)
            if data['form']['date_to']:
                res['to_date']=datetime.strptime(data['form']['date_to'],"%Y-%m-%d").strftime(lang_brw.date_format)
        else:
            print "1111111111111111111"
            if data['form']['date_from']:
                
                frm_date=data['form']['date_from']
                res['frm_date']=frm_date
            if  data['form']['date_to']:  
                to_date=data['form']['date_to']
                res['to_date']=to_date
        print "res==================",res
        return res
    
    @api.multi
    def get_detail_lines(self,data):
        print'multiiiiiiiii55555555555555555555',self,data
        self.cr.execute('select invoice_id from account_invoice_tax')
        acc_tax = self.cr.fetchall()
        if acc_tax:
            acc_tax_inv=[i[0] for i in acc_tax]
        taxes = self.pool.get('account.tax').search(self.cr,self.uid,[('type_tax_use','=','sale')])
        inv_lst=[]
        self.cr.execute("select id from account_move where state = 'posted'")
        acc_mov = self.cr.fetchall()
        move = self.pool.get('account.move').search(self.cr,self.uid,[])
        if acc_mov:
            acc_moves=[i[0] for i in acc_mov]
        if  acc_tax_inv:   
            print "acc_tax_inv======================",acc_tax_inv,move
            if data["form"]["date_from"] and data["form"]["date_to"]:
                end_date = data["form"]["date_to"]
                start_date = data["form"]["date_from"]
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        self.cr.execute("SELECT id from account_invoice  where date_invoice <= %s AND date_invoice >= %s AND id in %s AND move_id in %s",( end_date,start_date,tuple(acc_tax_inv),tuple(acc_moves),))
                        inv_res = self.cr.fetchall()
                else:
                    self.cr.execute("SELECT id from account_invoice  where  date_invoice <= %s AND date_invoice >= %s AND id in %s AND move_id in %s",( end_date,start_date,tuple(acc_tax_inv),tuple(move),))
                    inv_res = self.cr.fetchall()
            else:
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        self.cr.execute("SELECT id from account_invoice  where  id in %s AND move_id in %s",(tuple(acc_tax_inv),tuple(acc_moves),))
                        inv_res = self.cr.fetchall()
                else:
                    self.cr.execute("SELECT id from account_invoice  where  id in %s AND move_id in %s",(tuple(acc_tax_inv),tuple(move),))
                    inv_res = self.cr.fetchall()
                    print "inv_res========================",inv_res
            
            
            inv_lst=[i[0] for i in inv_res]
            print "inv_lst==============================",inv_lst
            tx_amt=0
            
        l=[]
        for tax in taxes:
            tot_amt=0
            result={}
            tax_brw=self.pool.get('account.tax').browse(self.cr,self.uid,tax)
            lst =[]
            tx_amt=0
            
            if inv_lst:
                self.cr.execute('select child_tax from account_tax_filiation_rel where parent_tax = %s',(tax,))
                child = self.cr.fetchall()
                if child:
                    child_taxes=[i[0] for i in child]
                else:
                    child_taxes=[tax]
                for mv in inv_lst:
                    tax_tot=0
                    self.cr.execute('select sum(amount) from account_invoice_tax where tax_id in %s AND invoice_id =  %s',(tuple(child_taxes),mv,))
                    tamt = self.cr.fetchall()
                    inv_brw=self.pool.get('account.invoice').browse(self.cr,self.uid,mv)
                    
                    if tamt:
                        tax_tot=tamt[0][0]
                        tot_amt=tot_amt+inv_brw.amount_untaxed
                        lang = self.pool.get('res.lang').search(self.cr,self.uid,[('code','=',inv_brw.partner_id.lang)])
                        if lang:
                            lang_brw=self.pool.get('res.lang').browse(self.cr,self.uid,lang[0])
                            inv_date=datetime.strptime(inv_brw.date_invoice,"%Y-%m-%d").strftime(lang_brw.date_format)
                        else:
                            inv_date=inv_brw.date_invoice
                        self.cr.execute("select sum(amount_untaxed) as subtotal  from account_invoice where move_id in (select id from account_move) and id in (select invoice_id from account_invoice_tax) and id in (select invoice_id from account_invoice_line where id in (select invoice_line_id from account_invoice_line_tax where tax_id =%s))",(tax,))
                        amts_dict = self.cr.dictfetchall()[0]
                    
                        if tax_tot:
                            tx_amt= tx_amt +tax_tot
                            lst.append({'inv_name':inv_brw.number,'tax':tax_tot,'tot':inv_brw.amount_untaxed,'partner':inv_brw.partner_id.name,'date':inv_date,'ref':inv_brw.origin})
                            
            if lst:
                result={'tx_amt': tx_amt,'inv':lst ,'tax_name':tax_brw.name,'tot_amt':amts_dict['subtotal']}
            if result:
                l.append(result)
        return l
    
    @api.multi
    def get_detail_purchase_lines(self,data):
        print',ultiiiiiiiiiiiiiiiiiiiii666666666666666',self,data
        self.cr.execute('select invoice_id from account_invoice_tax')
        acc_tax = self.cr.fetchall()
        if acc_tax:
            acc_tax_inv=[i[0] for i in acc_tax]
        taxes = self.pool.get('account.tax').search(self.cr,self.uid,[('type_tax_use','=','purchase')])
        inv_lst=[]
        self.cr.execute("select id from account_move where state = 'posted'")
        acc_mov = self.cr.fetchall()
        if acc_mov:
            acc_moves=[i[0] for i in acc_mov]
        
        move = self.pool.get('account.move').search(self.cr,self.uid,[])
        if acc_tax_inv:
            if data["form"]["date_from"] and data["form"]["date_to"]:
                end_date = data["form"]["date_to"]
                start_date = data["form"]["date_from"]
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        self.cr.execute("SELECT id from account_invoice  where   date_invoice <= %s AND date_invoice >= %s AND id in %s AND move_id in %s",( end_date,start_date,tuple(acc_tax_inv),tuple(acc_moves),))
                        inv_res = self.cr.fetchall()
                else:
                    self.cr.execute("SELECT id from account_invoice  where  date_invoice <= %s AND date_invoice >= %s AND id in %s AND move_id in %s",( end_date,start_date,tuple(acc_tax_inv),tuple(move),))
                    inv_res = self.cr.fetchall()
            else:
                if  data["form"]['target_move']== 'posted':
                    if acc_moves:
                        self.cr.execute("SELECT id from account_invoice  where   id in %s AND move_id in %s",(tuple(acc_tax_inv),tuple(acc_moves),))
                        inv_res = self.cr.fetchall()
                else:
                    self.cr.execute("SELECT id from account_invoice  where id in %s AND move_id in %s",(tuple(acc_tax_inv),tuple(move),))
                    inv_res = self.cr.fetchall()
            inv_lst=[i[0] for i in inv_res]
            
            l=[]
            for tax in taxes:
                tx_amt=0
                tot_amt=0
                result={}
                tax_brw=self.pool.get('account.tax').browse(self.cr,self.uid,tax)
                lst =[]
                tx_amt=0
                if inv_lst:
                    self.cr.execute('select child_tax from account_tax_filiation_rel where parent_tax = %s',(tax,))
                    child = self.cr.fetchall()
                    if child:
                        child_taxes=[i[0] for i in child]
                    else:
                        child_taxes=[tax]
                    for mv in inv_lst:
                        self.cr.execute('select sum(amount) from account_invoice_tax where tax_id in %s AND invoice_id =  %s',(tuple(child_taxes),mv,))
                        tamt = self.cr.fetchall()
                        inv_brw=self.pool.get('account.invoice').browse(self.cr,self.uid,mv)
                        tot_amt=tot_amt+inv_brw.amount_untaxed
                        if tamt:
                            tax_tot=tamt[0][0]
                            lang = self.pool.get('res.lang').search(self.cr,self.uid,[('code','=',inv_brw.partner_id.lang)])
                            if lang:
                                lang_brw=self.pool.get('res.lang').browse(self.cr,self.uid,lang[0])
                                inv_date=datetime.strptime(inv_brw.date_invoice,"%Y-%m-%d").strftime(lang_brw.date_format)
                            if tax_tot:
                                lst.append({'inv_name':inv_brw.number,'tax':tax_tot,'tot':inv_brw.amount_untaxed,'partner':inv_brw.partner_id.name,'date':inv_date,'ref':inv_brw.origin})
                                tx_amt= tx_amt +tax_tot
                    self.cr.execute("select sum(amount_untaxed) as subtotal  from account_invoice where move_id in (select id from account_move) and id in (select invoice_id from account_invoice_tax) and id in (select invoice_id from account_invoice_line where id in (select invoice_line_id from account_invoice_line_tax where tax_id =%s))",(tax,))
                    amts_dict = self.cr.dictfetchall()[0]
                    
                                   
                if lst:
                    result={'tx_amt': tx_amt,'inv':lst ,'tax_name':tax_brw.name,'tot_amt':amts_dict['subtotal']}
                if result:
                    l.append(result)
        return l
    
    @api.multi
    def get_sale_total(self,data):
        print'multiiiiii77777777777777777777'
        res={}
        sale_lines = self.get_lines(data)
        tot=0
        for sal in sale_lines:
            tot_tax = tot+sal['tax']
            tot = tot+sal['total']
            sub = tot+sal['subtot_amt']
        res = {'tot_tax':tot_tax,'tot':tot,'sub':sub}
        return res    
    
    @api.multi
    def get_purchase_total(self,data):
        print'multi888888888888888888888888888'
        res={}
        sale_lines = self.get_purchase_lines(data)
        tot=0
        for sal in sale_lines:
            tot_tax = tot+sal['tx_amt']
            tot = tot+sal['tot_amt']
            sub = tot+sal['subtot_amt']
        res = {'tot_tax':tot_tax,'tot':tot,'sub':sub}
        return res    
      
        
        
        
    
class report_tax_document_new(models.AbstractModel):
    _name = 'report.tax_report.report_tax'
    _inherit = 'report.abstract_report'
    _template = 'tax_report.report_tax'
    _wrapped_report_class = ReportTaxReport

