{
    "name" : "Statement Report Modifications",
    "version" : "1.0",
    "author" : "Pragmatic Techsoft Pvt. Ltd.",
    "category": "Report",
    "description": """
Ram Equipment - Statement Report Modifications
==============================================

""",
    "website": "www.pragtech.co.in",
    "depends" : [
                 "bulk_email","report","partner_statements"
                 ],
    "data": ['statement_report.xml',
             'custom_paperformat_layout.xml'
             ],
    "installable": True,
    "active": True,
}

