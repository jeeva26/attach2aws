# -*- coding: utf-8 -*-
{
    'name': 'Saasmate Reports',
    'version': '10.0',
    'category': 'Report',
    'description' : '''Designed reports for saasmate''',
    'author': "Pragmatic Techsoft Pvt. Ltd.",
    'website': 'www.pragtech.com.au',

    'depends': ['sale','account','stock','web_digital_sign'
    ],

    'data': [
             'views/base.xml',
             'views/res_company_view.xml',
#              'views/account_report.xml',
             'views/layout_templates.xml',
             'views/report_invoice.xml',
             'views/saasmate_reports.xml',
             'wizard/account_report_partner_ledger_view.xml',
             'views/res_partner_view.xml',
             'views/report_partnerledger.xml',
             'views/sale_report.xml',
             'views/report_saleorder_main.xml',
             'views/report_saleorder_main_without_image.xml',
             'reports/report_picking_list.xml',
             'reports/report_packingslip.xml',
             'views/sale_views.xml',
             'data/mail_template_data.xml',
             'views/report_purchaseorder.xml',
             'views/purchase_report.xml',
             ],
    'qweb': [
    ],
    'installable': True,
}
