# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from openerp.exceptions import ValidationError
import logging
_logger = logging.getLogger(__name__)

class StockPicking(models.Model):
    _inherit = "stock.picking"
    
    def get_client_order_ref(self, origin):
        """Take origin as an argument and returns client order reference of SO."""
        client_order_ref = self.env['sale.order'].search([('name','=',origin)], limit=1).client_order_ref
        return client_order_ref
    
    def get_sales_person(self, origin):
        """Take origin as an argument and returns sales person of SO"""
        user_id = self.env['sale.order'].search([('name','=',origin)], limit=1).user_id
        return user_id.name
    
    def get_invoice_address(self, origin):
        """Take origin as an argument and returns partner&#39;s billing address of SO."""
        partner_invoice_id = self.env['sale.order'].search([('name','=',origin)], limit=1).partner_invoice_id
        return partner_invoice_id
    
    def get_unit_price(self, origin, product_id):
        """This method take origin and product_id as an argument and return SO line level product unit price."""
        order = self.env['sale.order'].search([('name','=',origin)], limit=1)
        if order:
            line = self.env['sale.order.line'].search([('product_id','=',product_id)], limit=1).unit_price
        return line
    
    def get_back_order_qty(self, ordered_qty, done_qty):
        """Take ordered and done quantity as an
            argument and compute and return backorder quantity as ordered_qty – done_qty."""
        return ordered_qty - done_qty
    
    def get_product_description(self, product_id):
        """Remove code prefix from product name if present"""
        product = self.env['product.product'].search([('id','=',product_id)],limit=1)
        name = product.name_get()[0][1]
        flag = True
        tmp_name = ''
        if name and name[0]=='[':
            flag = False
            for c in name:
                if flag:
                    tmp_name += c
                if c == ']':
                    flag = True
        if tmp_name:
            name = tmp_name
            
        if product.description_sale:
            name += '\n' + product.description_sale
        return name
    
    @api.multi
    def do_print_picking(self):
        self.write({'printed': True})
        return self.env["report"].get_action(self, 'saasmate_reports.report_pickingslip')
StockPicking()    