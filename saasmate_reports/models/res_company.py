# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError
import os

class Company(models.Model):
    _inherit = "res.company"
    
    def _get_home_logo(self):
        """Return a base64 encoded string of home division logo"""
        return open(os.path.join(tools.config['root_path'], 'addons', 'base', 'res', 'res_company_logo.png'), 'rb') .read().encode('base64')

    def _get_health_logo(self):
        """Return a base64 encoded string of health division logo"""
        return open(os.path.join(tools.config['root_path'], 'addons', 'base', 'res', 'res_company_logo.png'), 'rb') .read().encode('base64')        
    
    home_logo = fields.Binary('Home', default=_get_home_logo)
    health_logo = fields.Binary('Health', default=_get_health_logo)