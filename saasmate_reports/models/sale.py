# -*- coding: utf-8 -*-

from odoo import api, models, fields


class SaleOrder(models.Model):
    _inherit = "sale.order"
    
    job_ref = fields.Char('Job Reference') #Used for printing on Quote report
    
    @api.multi
    def print_quotation(self):
        """This method is overridden to call new customized quotation report"""
        self.filtered(lambda s: s.state == 'draft').write({'state': 'sent'})
        return self.env['report'].get_action(self, 'saasmate_reports.report_saleorder_main')