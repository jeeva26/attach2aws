# -*- coding: utf-8 -*-

from . import stock
from . import account_invoice
from . import res_company
from . import res_partner
from . import sale
