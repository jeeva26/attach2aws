# -*- coding: utf-8 -*-

from odoo import api, models

class ResPartner(models.Model):
    _inherit = "res.partner"
    
    @api.one
    def getBankDetails(self, sender = 0, journal = False):
        res = {}
        res_partner_bank = self.env['res.partner.bank']
        bank_accounts = res_partner_bank.search([('partner_id','=',self.id)])
        if sender and journal:
            res = {}
            res['bsb'] = journal.bank_id.bic
            res['acc_number'] = journal.bank_acc_number
            return res
        if bank_accounts:
            res = {}
            res['bsb'] = bank_accounts[0].bank_id.bic
            res['acc_number'] = bank_accounts[0].acc_number
            return res
        return res