# -*- coding: utf-8 -*-
import math
from odoo import api, fields, models, _

class TxnCode(models.Model):
    _name="txn.code"
    
    name = fields.Char('Name')
    code = fields.Integer('Code')