# -*- coding: utf-8 -*-
from odoo import api, models, fields
from odoo.exceptions import ValidationError
from odoo.tools.translate import _

class AccountJournal(models.Model):
    _inherit = "account.journal"
    
    bank_acc_number = fields.Char('Account Number', size=22, help="15, 16, and 19 digits account numbers are acceptable.\n \
    Hyphens are optional. If hyphens are used, ensure they are positioned correctly.\n \
    Examples: \n \
    -Correct: 999999999999999 \n \
    -Correct: 9999999999999999 \n \
    -Correct: 9999999999999999999 \n \
    -Correct: 99-9999-9999999-99 \n \
    -Correct: 99-9999-9999999-999 \n \
    -Correct: 999-9999-999999999-999 ")
    
    @api.model
    def check_acc_no(self,acc_no):
        #Check account number validation
        if acc_no:
            flag = True
            length = len(acc_no)
            if (length == 18 or length == 19 or length == 22) and acc_no.find('-') != -1:
                if length == 18 and acc_no[2] == '-' and acc_no[7] == '-' and acc_no[-3] == '-':
                    flag = False
                elif length == 19 and acc_no[2] == '-' and acc_no[7] == '-' and acc_no[-4] == '-':
                    flag = False
                elif length == 22 and acc_no[3] == '-' and acc_no[8] == '-' and acc_no[-4] == '-':
                    flag = False
            if (length == 15 or length == 16 or length == 19) and acc_no.find('-') == -1:
                flag = False
            if flag:
                raise ValidationError(_("Account No. should be in acceptable format.\n \
                15, 16, and 19 digits account numbers are acceptable.\n \
                Hyphens are optional. If hyphens are used, ensure they are positioned correctly.\n \
                Examples: \n \
                -Correct: 999999999999999 \n \
                -Correct: 9999999999999999 \n \
                -Correct: 9999999999999999999 \n \
                -Correct: 99-9999-9999999-99 \n \
                -Correct: 99-9999-9999999-999 \n \
                -Correct: 999-9999-999999999-999"))  
            
    @api.model
    def create(self,vals):
        acc_no = vals.get('bank_acc_number')
        if acc_no:
            self.check_acc_no(acc_no)
        return super(AccountJournal, self).create(vals)
    
    @api.multi
    def write(self, vals):
        if 'bank_acc_number' in vals and vals['bank_acc_number']:
            self.check_acc_no(vals.get('bank_acc_number'))
        return super(AccountJournal, self).write(vals)
    
AccountJournal()


class ResPartnerBank(models.Model):
    _inherit = 'res.partner.bank'
    
    acc_number = fields.Char('Account Number', size=22, help="15, 16, and 19 digits account numbers are acceptable.\n \
    Hyphens are optional. If hyphens are used, ensure they are positioned correctly.\n \
    Examples: \n \
    -Correct: 999999999999999 \n \
    -Correct: 9999999999999999 \n \
    -Correct: 9999999999999999999 \n \
    -Correct: 99-9999-9999999-99 \n \
    -Correct: 99-9999-9999999-999 \n \
    -Correct: 999-9999-999999999-999 ")
    
    @api.model
    def create(self,vals):
        acc_no = vals.get('acc_number')
        self.env['account.journal'].check_acc_no(acc_no)
        return super(ResPartnerBank, self).create(vals)
    
    @api.multi
    def write(self, vals):
        if 'acc_number' in vals:
            self.env['account.journal'].check_acc_no(vals.get('acc_number'))
        return super(ResPartnerBank, self).write(vals)    
    
ResPartnerBank()
    