# -*- coding: utf-8 -*-
import math
from odoo import api, fields, models, _
from odoo.tools import amount_to_text_en, float_round
from odoo.exceptions import UserError, ValidationError
import datetime
import base64
import sys
import logging

_logger = logging.getLogger(__name__)

INV_TO_PARTN = {
    'out_invoice': 'customer',
    'out_refund': 'customer',
    'in_invoice': 'supplier',
    'in_refund': 'supplier',
}
# Since invoice amounts are unsigned, this is how we know if money comes in or
# goes out
INV_TO_PAYM_SIGN = {
    'out_invoice': 1,
    'in_refund': 1,
    'in_invoice': -1,
    'out_refund': -1,
}

class AccountRegisterPayments(models.TransientModel):
    _inherit = "account.register.payments"

    aba_file = fields.Many2one('ir.attachment',string="File")
    #Fields required for ABA generation    
    generate_aba_file = fields.Boolean(string="Generate file",default=True)
    hide_aba_fields = fields.Boolean('Hide ABA Fields',help="Hides ABA related fields",default=True)
            
    @api.multi
    def make_payments(self):
        # Make group data either for Customers or Vendors
        res = super(AccountRegisterPayments,self).make_payments()
        if self.generate_aba_file and not self.is_customer:
            user_id = self._uid
            company_details = self.env['res.users'].browse(user_id).partner_id.company_id
            if not self.journal_id.bank_acc_number:
                raise ValidationError(_("Bank account number is not defined for selected journal."))
            data = 'Payment Name,Date,Deduction Account,Amount,Payee Particulars,Payee Code,Payee Reference,Destination Account,Payer Particulars,Payer Code,Payer Reference,Payee Name\n'
            for payment_line in self.invoice_payments:
                date = datetime.datetime.strptime(self.payment_date,"%Y-%m-%d").strftime("%d/%m/%y")
                if payment_line.paying_amt > 0:
                    supplierBankDetails = payment_line.invoice_id.partner_bank_id
                    if not supplierBankDetails:
                        raise ValidationError(_("%s bank account number is not defined."%(payment_line.partner_id.name)))
                    data += str(self.communication[:20]) if self.communication else ''  
                    data += ','+str(date or '')+','+ str(self.journal_id.bank_acc_number)+','+ str(payment_line.paying_amt)
                    data += ','+str(company_details.name)+','+str(payment_line.partner_id.ref or '')+','+str(payment_line.payee_reference or '')+','+str(supplierBankDetails.acc_number or '')
                    data += ','+str(self.communication or '')+','+str(payment_line.partner_id.ref or '')+','+''
                    data += ','+str(payment_line.partner_id.name[:32]) if payment_line.partner_id else ''
                    data += '\n'
            out_data = False
            try:
                out_data = base64.encodestring(data)
            except Exception as ae:
                e = sys.exc_info()[0]
                _logger.warning('Error in generating file! (%s).', e)
                raise ValidationError(_(ae))
            if out_data:
                new_attach = self.env['ir.attachment'].create({
                    'name':"Batch Payment.csv",
                    'res_name': 'Payment ABA',
                    'type': 'binary',
                    'res_model': 'account.register.payments',
                    'datas':out_data,
                    'datas_fname':"Batch Payment.csv",
                })                   
                  
                self = self.with_context({'aba_file_id':new_attach.id})
                return {
                            'name' : 'Download File',
                            'type' : 'ir.actions.act_window',
                            'view_type' : 'form',
                            'view_mode' : 'form',
                            'res_model' : 'aba.file.download',
                            'target' : 'new',
                            'context' : self._context,
                        }
        return res

def AmountToTextFractional(amountInt):
    amount_word = amount_to_text_en.amount_to_text(
        math.floor(amountInt), lang='en', currency='')
    amount_word = amount_word.replace(' and Zero Cent', '')
    decimals = amountInt % 1
    if decimals >= 10**-2:
        amount_word += _(' and %s/100') % str(int(round(
            float_round(decimals*100, precision_rounding=1))))
    return amount_word

