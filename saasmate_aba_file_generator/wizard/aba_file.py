from odoo import api, fields, models, _

class aba_file_download(models.TransientModel):
    _name = "aba.file.download"
    
    aba_file = fields.Many2one('ir.attachment',string="ABA File")    
    
    @api.multi
    def action_download_aba_file(self):
        return {
                'type' : 'ir.actions.act_url',
                'url': '/web/binary/download_file?model=ir.attachment&field=datas&id=%s&filename=batch_payment.csv'% (self._context.get('aba_file_id')),
                'target': 'self',                
                }    