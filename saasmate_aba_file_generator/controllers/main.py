from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception,content_disposition
import base64 
class Binary(http.Controller):
    @http.route('/web/binary/download_file', type='http', auth="public")
    @serialize_exception
    def download_document(self,model,field,id,filename=None, **kw):
        """ Download link for files stored as binary fields.
        :param str model: name of the model to fetch the binary from
        :param str field: binary field
        :param str id: id of the record from which to fetch the binary
        :param str filename: field holding the file's name, if any
        :returns: :class:`werkzeug.wrappers.Response`
        """
        Model = request.env[model]
        filecontent = ''
        ir_attach_obj = Model.browse(int(id))
        res = ir_attach_obj.read([field])
        if res:
            filecontent = base64.b64decode(res[0][field] or '')   
        if not filecontent:
            return request.not_found()
        else:
            if not filename:
                filename = '%s_%s' % (model.replace('.', '_'), id) + '.csv'
            return request.make_response(filecontent,
                           [('Content-Type', 'application/octet-stream'),
                           ('Content-Disposition', content_disposition(filename))])