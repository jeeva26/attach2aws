# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http, _
from odoo.exceptions import AccessError
from odoo.http import request

from odoo.addons.website_portal.controllers.main import website_account


class amtech_website_account(website_account):

    @http.route()
    def account(self, **kw):
        """ Add sales documents to main account page """
        response = super(amtech_website_account, self).account(**kw)
        partner = request.env.user.partner_id

        SaleOrder = request.env['sale.order']
        Invoice = request.env['account.invoice']
        Claim = request.env['crm.claim']
        Picking = request.env['stock.picking']
        quotation_count = SaleOrder.search_count([
            ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
            ('state', 'in', ['sent', 'cancel'])
        ])
        order_count = SaleOrder.search_count([
            ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
            ('state', 'in', ['sale', 'done'])
        ])
        invoice_count = Invoice.search_count([
            ('type', 'in', ['out_invoice', 'out_refund']),
            ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
            ('state', 'in', ['open', 'paid', 'cancel'])
        ])
        claim_count = Claim.search_count([
             ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
        ])        
        backorder_count = Picking.search_count([
            ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
             ('backorder_id', '!=', False),
             ('state', 'not in',['cancel','draft'] ),
        ])             
        response.qcontext.update({
            'quotation_count': quotation_count,
            'order_count': order_count,
            'invoice_count': invoice_count,
            'claim_count':claim_count,
            'backorder_count':backorder_count,
        })
        return response
    
    @http.route(['/my/claims', '/my/claims/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_claims(self, page=1, date_begin=None, date_end=None, **kw):
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        CrmClaims = request.env['crm.claim']

        domain = [
            ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
        ]
        archive_groups = self._get_archive_groups('crm.claim', domain)
        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]

        # count for pager
        claim_count = CrmClaims.search_count(domain)
        # pager
        pager = request.website.pager(
            url="/my/claims",
            url_args={'date_begin': date_begin, 'date_end': date_end},
            total=claim_count,
            page=page,
            step=self._items_per_page
        )
        # content according to pager and archive selected
        claims = CrmClaims.search(domain, limit=self._items_per_page, offset=pager['offset'])
        values.update({
            'date': date_begin,
            'claims': claims,
            'page_name': 'Claim',
            'pager': pager,
            'archive_groups': archive_groups,
            'default_url': '/my/claims',
        })
        return request.render("amtech_portal.amtech_my_claims", values)    
    
    @http.route(['/my/claims/pdf/<int:claim_id>'], type='http', auth="user", website=True)
    def portal_get_claim(self, claim_id=None, **kw):
        claim = request.env['crm.claim'].browse([claim_id])
        try:
            claim.check_access_rights('read')
            claim.check_access_rule('read')
        except AccessError:
            return request.render("website.403")

        pdf = request.env['report'].sudo().get_pdf([claim_id], 'crm_claim.report_crmclaim')
        pdfhttpheaders = [
            ('Content-Type', 'application/pdf'), ('Content-Length', len(pdf)),
            ('Content-Disposition', 'attachment; filename=Claim.pdf;')
        ]
        return request.make_response(pdf, headers=pdfhttpheaders)    
    
    @http.route(['/my/backorders', '/my/backorders/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_backorders(self, page=1, date_begin=None, date_end=None, **kw):
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        Backorders = request.env['stock.picking']

        domain = [
             ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
             ('backorder_id', '!=', False),
             ('state', 'not in',['cancel','draft'] ),            
        ]
        archive_groups = self._get_archive_groups('stock.picking', domain)
        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]

        # count for pager
        backorder_count = Backorders.search_count(domain)
        # pager
        pager = request.website.pager(
            url="/my/backorders",
            url_args={'date_begin': date_begin, 'date_end': date_end},
            total=backorder_count,
            page=page,
            step=self._items_per_page
        )
        # content according to pager and archive selected
        backorders = Backorders.search(domain, limit=self._items_per_page, offset=pager['offset'])
        values.update({
            'date': date_begin,
            'backorders': backorders,
            'page_name': 'Backorder',
            'pager': pager,
            'archive_groups': archive_groups,
            'default_url': '/my/backorders',
        })
        return request.render("amtech_portal.amtech_my_backorders", values)   
    
    @http.route(['/my/backorders/<int:picking_id>'], type='http', auth="user", website=True)
    def backorder_followup(self, picking_id=None, **kw):
        picking = request.env['stock.picking'].browse([picking_id])
        try:
            picking.check_access_rights('read')
            picking.check_access_rule('read')
        except AccessError:
            return request.render("website.403")

        picking_sudo = picking.sudo()
        order_picking_lines = {il.product_id.id: il.location_id for il in picking_sudo.pack_operation_product_ids}
        return request.render("amtech_portal.backorders_followup", {
            'order': picking_sudo,
            'picking_lines': order_picking_lines,
        })    