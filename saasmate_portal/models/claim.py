from odoo.tools.translate import _
from odoo import api,fields, models

class crm_claim(models.Model):
    """ Crm claim
    """
    _inherit = "crm.claim"
    _description = "Claim"
    
    @api.model
    def create(self, vals):
        res = super(crm_claim, self).create(vals)
        for claim in res:
            if claim.partner_id not in claim.message_partner_ids:
                claim.message_subscribe([claim.partner_id.id])        
        return res    