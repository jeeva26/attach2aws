# -*- coding: utf-8 -*-

{'name': 'RMA Claims by shop',
'author' : 'Pragtech Techsoft Pvt Ltd',
'version': '1.0',   
'website': 'http://pragtech.co.in',
 'category': 'Generic Modules/CRM & SRM',
 'depends': ['crm_claim', 'sale'
             ],
 'description': """
RMA Claim by shops
==================

Claim improvements to use them by shops:

 * Add shop on claim
 * Add various filter in order to work on a basic "by shop" basis

 Was originally designed for e-commerce purpose, but could probably do the trick
 for other cases as well.

""",
 'images': [],
 'demo': [],
 'data': [
    'claim_view.xml',
 ],
 'installable': True,
 'application': True,
}
