# -*- coding: utf-8 -*-
from odoo import _, api, exceptions, fields, models

class crm_claim(models.Model):
    _inherit = 'crm.claim'

    name = fields.Char(related='categ_id.name')
#     name = fields.related(
#             'categ_id',
#             'name',
#             relation='crm.case.categ',
#             type='char',
#             string='Claim Subject',
#             size=128,
#             store=True)
    categ_id = fields.Many2one(
            'crm.case.categ',
            'Category',
            domain="[('section_id', '=', section_id), \
                    ('object_id.model', '=', 'crm.claim')]",
            required=True)
