# -*- coding: utf-8 -*-

class ProductNoSupplier(Exception):
    """ Raised when a warranty cannot be computed for a claim line
    because the product has no supplier.
    """
