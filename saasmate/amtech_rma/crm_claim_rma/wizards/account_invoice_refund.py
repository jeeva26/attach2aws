# -*- coding: utf-8 -*-
from odoo import api, fields, models

class AccountInvoiceRefund(models.TransientModel):
    _inherit = "account.invoice.refund"

    def _default_description(self):
        return self.env.context.get('description', '')

    description = fields.Char(default=_default_description)

    @api.multi
    def compute_refund(self, mode='refund'):
        self.ensure_one()
        invoice_ids = self.env.context.get('invoice_ids', [])
        if invoice_ids:
            self = self.with_context(active_ids=invoice_ids)
        return super(AccountInvoiceRefund, self).compute_refund(mode=mode)
