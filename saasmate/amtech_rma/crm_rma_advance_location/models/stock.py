# -*- coding: utf-8 -*-
from odoo import api, fields, models

class stock_warehouse(models.Model):

    _inherit = "stock.warehouse"

    lot_carrier_loss_id = fields.Many2one(
            'stock.location',
            'Location Carrier Loss')
    lot_breakage_loss_id = fields.Many2one(
            'stock.location',
            'Location Breakage Loss')
    lot_refurbish_id = fields.Many2one(
            'stock.location',
            'Location Refurbish')
