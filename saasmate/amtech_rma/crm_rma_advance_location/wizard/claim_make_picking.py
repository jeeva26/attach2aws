# -*- coding: utf-8 -*-
from odoo import api, fields, models

class claim_make_picking(models.TransientModel):

    _inherit = 'claim_make_picking.wizard'

    @api.model
    def _get_dest_loc(self):
        """ Get default destination location """
        loc_id = super(claim_make_picking, self)._get_dest_loc()
        if self._context is None:
            context = {}
        warehouse_obj = self.pool.get('stock.warehouse')
        warehouse_id = self._context.get('warehouse_id')
        if self._context.get('picking_type') == 'in':
            loc_id = warehouse_obj.read(warehouse_id,['lot_rma_id'])['lot_rma_id'][0]
        elif context.get('picking_type') == 'loss':
            loc_id = warehouse_obj.read(warehouse_id,['lot_carrier_loss_id'])['lot_carrier_loss_id'][0]
        return loc_id

    _defaults = {
        'claim_line_dest_location': _get_dest_loc,
    }
