# -*- coding: utf-8 -*-
{
    'name': 'Product warranty',
    'category': 'Generic Modules/Product',
    'author': 'Pragmatic Techsoft Pvt. ltd.',
    'website': 'www.pragtech.co.in',
    'version': '1.0',   
    'description': """
    Product Warranty

""",    
    'depends': ['product','sale'],
    'data': [
        'security/ir.model.access.csv',
        'views/res_company.xml',
        'views/product_warranty.xml',
    ],
    'demo': [
        'demo/product_warranty.xml',
        'demo/res_company.xml',
    ],
    'test': [],
    'installable': True,
    'active': False,
    'images': ['images/product_warranty.png'],
}
