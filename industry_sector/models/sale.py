# -*- coding: utf-8 -*-
from odoo import models, fields, api

class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    @api.depends('partner_id.industrysector_id')
    def _compute_industrysector(self):
        for line in self:
            if line.partner_id:
                line.industrysector_id=line.partner_id.industrysector_id.id
                
    industrysector_id = fields.Many2one('industry.sector',compute='_compute_industrysector',store=True)