# -*- coding: utf-8 -*-
from odoo import models, fields

class res_partner(models.Model):
    _inherit = 'res.partner'
    
    industrysector_id = fields.Many2one('industry.sector',"Customer Category")