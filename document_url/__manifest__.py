# -*- coding: utf-8 -*-
{
    'name': 'URL attachment',
    'version': '10.0',
    'category': 'Tools',
    'author': "Pragmatic,"
              "Odoo Community Association (OCA)",
    'website': 'www.pragtech.com.au',
    'license': 'AGPL-3',
    'depends': [
        'document',
    ],
    'data': [
        'view/document_url_view.xml',
    ],
    'qweb': [
        'static/src/xml/url.xml',
    ],
    'installable': True,
}
