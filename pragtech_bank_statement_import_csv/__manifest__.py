{
    'name': 'Bank Statements Import',
    'category': 'Accounting & Finance',
    'summary': 'Import Bank Statements in csv format',
    'website': 'www.pragtech.co.in',
    'version': '1.0',
    'author': 'Pragmatic Techsoft Pvt. Ltd.',
    'description': """
This module allows you to import CSV Files.
    """,
    'data': [
        'wizard/pragtech_bank_statement_import_csv.xml',
    ],
    'depends': [
        'account_bank_statement_import',
    ],
    'installable': True,
    'application': False,
}

