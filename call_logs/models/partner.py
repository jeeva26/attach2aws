from odoo import fields, models, api, _
from email.utils import formataddr
from lxml import etree

ADDRESS_FORMAT_CLASSES = {
    '%(city)s %(state_code)s\n%(zip)s': 'o_city_state',
    '%(zip)s %(city)s': 'o_zip_city'
}


class FormatAddress(object):

    @api.model
    def fields_view_get_address(self, arch):
        address_format = self.env.user.company_id.country_id.address_format or ''
        for format_pattern, format_class in ADDRESS_FORMAT_CLASSES.iteritems():
            if format_pattern in address_format:
                doc = etree.fromstring(arch)
                for address_node in doc.xpath("//div[@class='o_address_format']"):
                    # add address format class to address block
                    address_node.attrib['class'] += ' ' + format_class
                    if format_class.startswith('o_zip'):
                        zip_fields = address_node.xpath("//field[@name='zip']")
                        city_fields = address_node.xpath(
                            "//field[@name='city']")
                        if zip_fields and city_fields:
                            # move zip field before city field
                            city_fields[0].addprevious(zip_fields[0])
                arch = etree.tostring(doc)
                break
        return arch


class Partner(models.Model, FormatAddress):
    _description = 'Partner'
    _inherit = "res.partner"

    phone_count = fields.Integer(
        compute='_phone_count', string='# Phone Calls')
    email_count = fields.Integer(
        compute='_email_count', string='# Emails')
    task_count = fields.Integer(
        compute='_task_count', string='# Tasks')
    
    @api.one
    def _phone_count(self):
        call_id = self.env['crm.activity.log']
        call_obj = self.env.ref('crm.crm_activity_data_call')
        count = call_id.search_count(
            [('next_activity_id', '=', call_obj[0].id), ('partner_id', '=', self.id)])
        self.phone_count = count
        
    @api.one
    def _email_count(self):
        email_id = self.env['crm.activity.log']
        email_obj = self.env.ref('crm.crm_activity_data_email')
        count_email = email_id.search_count(
            [('next_activity_id', '=', email_obj[0].id), ('partner_id', '=', self.id)])
        self.email_count = count_email
        
    @api.one
    def _task_count(self):
        task_id = self.env['crm.activity.log']
        task_obj = self.env.ref('crm.crm_activity_data_meeting')
        count = task_id.search_count(
            [ ('next_activity_id', '=', task_obj[0].id), ('partner_id', '=', self.id)])
        self.task_count = count
   

    @api.multi
    def call_logs(self):
        call_id = self.env.ref('crm.crm_activity_data_call')
        res = []
        if call_id:
            res = [each.id for each in self.env['crm.activity.log'].search(
                [('next_activity_id', '=', call_id[0].id), ('partner_id', '=', self.id)])]
            
        view_id = self.env.ref('call_logs.crm_activity_log_view_form')
        tree_view_id = self.env.ref('call_logs.crm_activity_log_view_tree_call')
        return {
            'name': _('Phone Call History'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.activity.log',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', res)],
            'views': [(tree_view_id.id, 'tree'),(view_id.id, 'form')],
            #                 'target':'new'
        }

    @api.multi
    def email_logs(self):
        email_id = self.env.ref('crm.crm_activity_data_email')
        res = []
        if email_id:
            res = [each.id for each in self.env['crm.activity.log'].search(
                [('next_activity_id', '=', email_id[0].id), ('partner_id', '=', self.id)])]
            
        view_id = self.env.ref('call_logs.crm_activity_log_view_form')
        tree_view_id = self.env.ref('call_logs.crm_activity_log_view_tree_email_task')
        return {
            'name': _('Email History'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.activity.log',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', res)],
            'views': [(tree_view_id.id, 'tree'),(view_id.id, 'form')],
        }

    @api.multi
    def task_logs(self):
        task_id = self.env.ref('crm.crm_activity_data_meeting')
        res = []
        if task_id:
            res = [each.id for each in self.env['crm.activity.log'].search(
                [('next_activity_id', '=', task_id[0].id), ('partner_id', '=', self.id)])]
            
        view_id = self.env.ref('call_logs.crm_activity_log_view_form')
        tree_view_id = self.env.ref('call_logs.crm_activity_log_view_tree_email_task')
        return {
            'name': _('Tasks History'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.activity.log',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', res)],
            'views': [(tree_view_id.id, 'tree'),(view_id.id, 'form')],
        }

    @api.multi
    def schedule_call(self):
        call_id = self.env.ref('crm.crm_activity_data_call')
        res = self.env['crm.activity.log'].search(
            [('next_activity_id', '=', call_id[0].id), ('partner_id', '=', self.id)])
        if not res and call_id:
            res = self.env['crm.activity.log'].create(
                {'next_activity_id': call_id[0].id, 'flag': True, 'partner_id': self.id})
        if res:
            return res[-1].action_log_and_schedule()
