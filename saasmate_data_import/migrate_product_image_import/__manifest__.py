# -*- coding: utf-8 -*-

{
    "name": "Import Product Images",
    "version": "1.0",
    "depends": [
                'base_migrate',
                'product'
                ],
    "author": "Pragmatic Techsoft Pvt. Ltd.",
    "category": "General",
    "description": """
Imports Product Images
===========================

Custom Importer
    """,
    "data": [],
    "website": "http://www.pragtech.co.in",
    "demo": [],
    "test": [],
    "data": [
        "wizard/image_import_wizard_view.xml",
        "views/product_image_import_view.xml",
        #'security/ir.model.access.csv',
    ],
    "installable": True,
    "active": False,
}
