# -*- coding: utf-8 -*-

{
    "name": "Partner Import",
    "version": "1.0",
    "author": "Pragmatic Techsoft Pvt. Ltd.",
    "category": "General",
    "description": """
Imports partner
===================

Custom Importer for importing Partners

""",
    "depends": [
        'base_migrate',
        "product",
    ],
    'data': [
        "views/partner_import_view.xml",
        "wizard/partner_import_wizard_view.xml",
        "security/ir.model.access.csv"
    ],
    "demo": [],
    "test": [],
    "installable": True,
    "active": False,
}
