# -*- coding: utf-8 -*-

from openerp import fields, models
from operator import attrgetter

class partner_import(models.Model):
    _name = "migration.partner.import"
     
    processed = fields.Boolean(string="Processed")
    problem = fields.Char(string="Problem")
    created_partner_ids = fields.Many2many('res.partner')

    #Partner Import Data
    name = fields.Char(string="Name")
    is_a_company = fields.Boolean(string="Is a Company")
    street = fields.Char(string="Street")
    street2 = fields.Char(string="Street2")
    city = fields.Char(string="City")
    postcode = fields.Char(string="Postcode")
    state = fields.Char(string="State")
    country = fields.Char(string="Country")
    email = fields.Char(string="Email")
    website = fields.Char(string="Website")
    phone = fields.Char(string="Phone")
    mobile = fields.Char(string="Mobile")
    fax = fields.Char(string="Fax")
    is_supplier = fields.Boolean(string="Supplier")
    is_customer = fields.Boolean(string="Customer")
    parent_company = fields.Char(string="Parent Company")
    use_parent_address = fields.Boolean(string="Use Parent Address")
    contact_title = fields.Char(string="Contact Title")
    comment = fields.Text(string="Comment")
    tags = fields.Text(string='Tags')
    sale_pricelist = fields.Char(string="Sale Pricelist")
    purchase_pricelist = fields.Char(string="Purchase Pricelist")
    type = fields.Char(string="Contact Type")
    company = fields.Char(string="Company")
    ref = fields.Char(string="Internal Reference")
    
    #custom field import
    classification_id = fields.Char(string='Classification')
    sales_executive = fields.Char(string='Sales Executive')
    abn_number = fields.Char(string='ABN') #used for displaying on tax code
    template = fields.Char(string="Template")
    industrysector_id = fields.Char(string="Customer Category")
    over_credit = fields.Boolean(string='Allow Over Credit?')
    statement_config_id = fields.Char(string='Overriding Statement Cycle')
    
    def create_or_update_partners(self, dry_run=True, create_titles=False, create_tags=False):
        """Create or update parteners"""
        
        imported_names = []
        
        for stage in self.sorted(key=attrgetter('is_a_company'),reverse=True).filtered(lambda s: not s.processed):

            stage.write({'problem': False,
                         })
            
            title_data = {}
            
            partner_data = {
                            'street' : stage.street,
                            'street2' : stage.street2,
                            'city' : stage.city,
                            'zip' : stage.postcode,
                            'website' : stage.website,
                            'phone' : stage.phone,
                            'mobile' : stage.mobile,
                            'fax' : stage.fax,
                            'email' : stage.email,
                            'supplier' : stage.is_supplier,
                            'customer' : stage.is_customer,
                            'is_company' : stage.is_a_company,
                            'comment': stage.comment,
                            'ref':stage.ref,
                            #custome fields
                            'abn': stage.abn_number,
                            'over_credit':stage.over_credit,
                            }
             
            if stage.name:
                partner_data['name'] = stage.name
            else:
                stage.write({'problem': 'Partner Name not given'})
                continue
            
            if stage.country:
                country_search = self.env['res.country'].search([('name','=',stage.country)], limit=1)
                if country_search:
                    partner_data['country_id'] = country_search.id
                else:
                    stage.write({'problem': 'Country %s does not exist' % stage.country})
                    continue
                
            if stage.state:
                if partner_data.get('country_id'):
                    state_search = self.env['res.country.state'].search(["|",('name', '=', stage.state ),('code','=',stage.state),('country_id','=',partner_data['country_id'])], limit=1)
                    if state_search:
                        partner_data['state_id'] = state_search.id
                    else:
                        stage.write({'problem': 'State %s does not exist in the country %s' % (stage.state,stage.country)})
                        continue
                else:       
                    state_search = False
                    country_search = self.env['res.country'].search([('name','=','Australia')],limit=1)
                    if country_search:
                        state_search = self.env['res.country.state'].search(["|",('name', '=', stage.state ),('code','=',stage.state),('country_id','=',country_search.id)], limit=1)
                    if not state_search:
                        state_search = self.env['res.country.state'].search(["|",('name', '=', stage.state ),('code','=',stage.state)],limit=1)
                    if state_search:
                        partner_data['state_id'] = state_search.id
                    else:
                        stage.write({'problem': 'State %s does not exist' % (stage.state,)})
                        continue
            
            if stage.sale_pricelist:
                pricelist_search = self.env['product.pricelist'].search([('name','=',stage.sale_pricelist),('type','=','sale')],limit=1)
                if pricelist_search:
                    partner_data['property_product_pricelist'] = pricelist_search.id
                else:
                    stage.write({'problem': 'Pricelist %s does not exist' % stage.sale_pricelist})
                    continue
                    
            if stage.purchase_pricelist:
                pricelist_search = self.env['product.pricelist'].search([('name','=',stage.purchase_pricelist),('type','=','purchase')],limit=1)
                if pricelist_search:
                    partner_data['property_product_pricelist_purchase'] = pricelist_search.id
                else:
                    stage.write({'problem': 'Pricelist %s does not exist' % stage.purchase_pricelist})
                    continue
            
            if stage.tags:
                tag_ids = []
                non_existant_tags = []
                tags = stage.tags.split()
                tags = [tag.strip() for tag in tags]
                for tag in tags:
                    tag_search = self.env['res.partner.category'].search([('name','=',tag)])
                    if tag_search:
                        tag_ids.append(tag_search.id)
                    else:
                        if not create_tags:
                            non_existant_tags.append(tag)
                        
                        if not dry_run and create_tags:
                            tag_id = self.env['res.partner.category'].create({'name': tag})
                            tag_ids.append(tag_id.id)
                if non_existant_tags:
                    stage.write({'problem': 'Tag %s does not exist. Check Create Tags on the wizard to create the contact tags that dont already exist.' % ",".join(non_existant_tags)})
                    continue
                else:
                    partner_data['category_id'] = [(6,0,tag_ids)]
            
            if stage.contact_title:
                    
                title_search = self.env['res.partner.title'].search([('name','=ilike',stage.contact_title)])
                if title_search:
                        partner_data['title'] = title_search.id
                
                else:    
                
                    if not create_titles:
                        stage.write({'problem': 'Title %s does not exist. Check Create Titles on the wizard to create the contact titles that dont already exist.' % stage.contact_title})
                        continue
                    
                    if not dry_run:
                        title_data['name'] = stage.contact_title
                        new_title = self.env['res.partner.title'].create(title_data)
                        partner_data['title'] = new_title.id
            
            if stage.type:
                
                if stage.type in ('contact','invoice','delivery','other'):
                    partner_data['type'] = stage.type
                elif stage.type in ('Contact'):
                    partner_data['type'] = 'contact'
                elif stage.type in ('Invoice address'):
                    partner_data['type'] = 'invoice'
                elif stage.type in ('Shipping address'):
                    partner_data['type'] = 'delivery'
                elif stage.type in ('Other address'):
                    partner_data['type'] = 'other'
                else:
                    stage.write({'problem': 'Partner type %s does not exist' % stage.type})
                    continue
            
            if not stage.is_a_company and stage.parent_company:
                parent = self.env['res.partner'].search([('name', '=', stage.parent_company )])
                
                if len(parent) > 1:
                    stage.write({'problem': 'Parent Company "%s" has more than one record in the system. Please consolidate multiple records into one to continue.' % stage.parent_company})
                    continue
                
                if len(parent) == 1:
                    partner_data['parent_id'] = parent.id
                    partner_data['use_parent_address'] = stage.use_parent_address
                    
                else:
                    if not dry_run: 
                        stage.write({'problem': 'Parent Company does not exist'})
                        continue
                    
                    if dry_run and not stage.parent_company in imported_names:
                        stage.write({'problem': 'Parent Company does not exist'})
                        continue


            if stage.company and stage.company not in ('all','All'):
                company_search = self.env['res.company'].search([('name','=',stage.company)])
                
                if len(company_search) > 1:
                    stage.write({'problem': 'Company "%s" has more than one record in the system. Please consolidate multiple records into one to continue.' % stage.company})
                    continue
                
                if len(company_search) == 1:
                    partner_data['company_id'] = company_search.id
                else:
                    stage.write({'problem': 'Company %s does not exist' % stage.company})
                    continue
            elif stage.company in ('all','All'):
                partner_data['company_id'] = False
                
            #custom partner fields    
            if stage.sales_executive:
                sales_executive_search = self.env['res.users'].search([('name','=',stage.sales_executive)], limit=1)
                if sales_executive_search:
                    partner_data['sales_executive'] = sales_executive_search.id
#                 else:
#                     stage.write({'problem': 'Sales executive %s does not exist' % stage.sales_executive})
#                     continue
                
#             if stage.template:
#                 if stage.template in ('home_division','health_division'):
#                     partner_data['template'] = stage.template
#                 elif stage.template in ('Home Division'):
#                     partner_data['template'] = 'home_division'
#                 elif stage.template in ('Health Division'):
#                     partner_data['template'] = 'health_division'
#                 else:
#                     stage.write({'problem': 'Partner template %s does not exist' % stage.template})
#                     continue
                
            if stage.classification_id:
                classification_search = self.env['res.partner.classification'].search([('name','=',stage.classification_id)], limit=1)
                if classification_search:
                    partner_data['classification_id'] = classification_search.id
#                 else:
#                     stage.write({'problem': 'Partner classification %s does not exist' % stage.classification_id})
#                     continue
                
            if stage.industrysector_id:
                industrysector_search = self.env['industry.sector'].search([('name','=',stage.industrysector_id)], limit=1)
                if industrysector_search:
                    partner_data['industrysector_id'] = industrysector_search.id
#                 else:
#                     stage.write({'problem': 'Industry sector %s does not exist' % stage.industrysector_id})
#                     continue
            
            if stage.statement_config_id:
                statement_search = self.env['account.partner.statement.config'].search([('name','=',stage.statement_config_id)], limit=1)
                if statement_search:
                    partner_data['statement_config_id'] = statement_search.id
#                 else:
#                     stage.write({'problem': 'Statement Configuration %s does not exist' % stage.statement_config_id})
#                     continue
            

            imported_names.append(stage.name)
                
            if not dry_run:
                new_partner = self.env['res.partner'].create(partner_data)
        
            if not dry_run:
                stage.write({'processed': True, 'created_partner_ids' : new_partner.ids})    