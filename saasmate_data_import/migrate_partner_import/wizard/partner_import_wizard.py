# -*- coding: utf-8 -*-

from openerp import models, fields, api, _

class partner_import_wizard(models.TransientModel):
    _name="migration.partner.import.wizard"
    
    def _get_active_ids(self):
        return self.env.context.get("active_model") == "migration.partner.import" and self.env.context.get("active_ids", False) or False
    
    def _get_active_ids_len(self):
        return self.env.context.get("active_model") == "migration.partner.import" and len(self.env.context.get("active_ids", [])) or 0
    
    partner_import_ids = fields.Many2many("migration.partner.import", "partner_import_wizard_rel", "wizard_id", "stage_id", string="Import Data", default=_get_active_ids)
    dry_run = fields.Boolean(string="Check Only - Do not Update", default=True)
    create_titles = fields.Boolean(string="Create Contact Titles", default=False)
    create_tags = fields.Boolean(string="Create Contact Tags", default=False)
    count_partner_import = fields.Integer(string="Number of Selected Records", default=_get_active_ids_len)
    
    @api.multi
    def do_bulk_import(self):
        for wiz in self:
            wiz.partner_import_ids.create_or_update_partners(dry_run = wiz.dry_run, create_titles = wiz.create_titles, create_tags = wiz.create_tags)

        return {'type': 'ir.actions.act_window_close'}
    
