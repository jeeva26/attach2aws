# -*- coding: utf-8 -*-

from openerp import models, fields, api, _

class ProductVariantImportWizard(models.TransientModel):
    _name = "migration.bulk.product.variant.import.wizard"

    def _get_active_ids(self):
        return self.env.context.get("active_model") == "migration.product.variant.import.stage" and self.env.context.get("active_ids", False) or False
    def _get_active_ids_len(self):
        return self.env.context.get("active_model") == "migration.product.variant.import.stage" and len(self.env.context.get("active_ids", [])) or 0

    product_variant_import_stage_ids = fields.Many2many("migration.product.variant.import.stage", "bulk_product_variant_import_wizard_stage_rel", "wizard_id", "stage_id", string="Import Data", default=_get_active_ids)
    count_product_variant_import_stage = fields.Integer(string="Number of Selected Records", default=_get_active_ids_len)
    dry_run = fields.Boolean(string="Check Only - Do not Update", default=True)

    @api.multi
    def do_bulk_import(self):
        for wiz in self:
            wiz.product_variant_import_stage_ids.create_or_update_variants(
                                                                           dry_run = wiz.dry_run,
                                                                           )

        return {'type': 'ir.actions.act_window_close'}
