{
    "name": "Import Products and Variants",
    "version": "1.0",
    "depends": [
                "stock_account",
                'purchase',
                'base_migrate'
                ],
    "author": "Pragmatic Techsoft Pvt. Ltd.",
    "category": "General",
    "description": """
Imports Products and Variant
===============================

Custom Importer
    """,
    "data": [],
    "website": "http://www.pragtech.co.in",
    "demo": [],
    "test": [],
    "data": [
        "views/product_template_view.xml",
        "views/product_variant_view.xml",
        "wizard/bulk_process_product_template_wizard_view.xml",
        "wizard/bulk_process_product_variant_wizard_view.xml",
        'security/ir.model.access.csv',
    ],
    "installable": True,
    "active": False,
}
