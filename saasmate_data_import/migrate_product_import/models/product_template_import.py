# -*- coding: utf-8 -*-

from openerp import models, fields
import openerp.addons.decimal_precision as dp

class ProductTemplateImportStage(models.Model):
    _name = "migration.product.template.import.stage"

    processed = fields.Boolean(string="Processed")
    problem = fields.Char(string="Problem")
    created_product_ids = fields.Many2many('product.product')

    internal_ref = fields.Char(string='Internal Reference')
    name = fields.Char(string='Name')
    description = fields.Char(string='Description')
    barcode = fields.Char(string='Barcode')
    sale_ok = fields.Boolean(string='Can be Sold')
    purchase_ok = fields.Boolean(string='Can be Purchased')
    product_type = fields.Selection([('consu', 'Consumable'),('service','Service'),('product','Product')], string='Product Type')
    category = fields.Char(string='Category')
    weight = fields.Float(string='Gross Weight', digits=dp.get_precision('Stock Weight'))
    volume = fields.Float(string='Volume')

    company = fields.Char(string="Company")

    reorder_min = fields.Float(string='Reorder Min', digits=dp.get_precision('Product Unit of Measure'))
    reorder_max = fields.Float(string='Reorder Max', digits=dp.get_precision('Product Unit of Measure'))
    reorder_mult = fields.Float(string='Reorder Multiple', digits=dp.get_precision('Product Unit of Measure'))

    sale_delay = fields.Integer(string='Customer Lead Time')
    warranty = fields.Float(string='Warranty')
    list_price = fields.Float(string='Public Price', digits=dp.get_precision('Product Price'))
    standard_price = fields.Float(string='Cost Price', digits=dp.get_precision('Product Price'))
    uom = fields.Char(string='Unit of Measure')
    purchase_uom = fields.Char(string='Purchase UoM')
    sale_uom = fields.Char(string='Sale UoM')
    tracking = fields.Selection([('serial', 'By Unique Serial Number'), ('lot', 'By Lots'), ('none', 'No Tracking')], string='Tracking')
    lots = fields.Char(string='Lots')
    invoice_policy = fields.Selection(
        [('order', 'Ordered quantities'),
         ('delivery', 'Delivered quantities'),
         ('cost', 'Invoice based on time and material')],
        string='Invoicing Policy')


    supplier = fields.Char(string='Supplier Name')
    supplier_del = fields.Integer(string='Supplier Lead Time')
    supplier_min = fields.Float(string='Supplier Min Quantity')
    supplier_product_name = fields.Char(string='Supplier Product Name')
    supplier_product_code = fields.Char(string='Supplier Product Code')
    supplier_price = fields.Float(string='Supplier Price')
    supplier_currency = fields.Char(string='Supplier Currency')

    customer_tax = fields.Char(string='Customer Tax')
    supplier_tax = fields.Char(string='Supplier Tax')

    attrib_name_1 = fields.Char(string='Attrib 1 Name')
    attrib_values_1 = fields.Char(string='Attrib 1 Values')
    attrib_suffixes_1 = fields.Char(string='Code Suffixes 1')
    attrib_name_2 = fields.Char(string='Attrib 2 Name')
    attrib_values_2 = fields.Char(string='Attrib 2 Values')
    attrib_suffixes_2 = fields.Char(string='Code Suffixes 2')
    attrib_name_3 = fields.Char(string='Attrib 3 Name')
    attrib_values_3 = fields.Char(string='Attrib 3 Values')
    attrib_suffixes_3 = fields.Char(string='Code Suffixes 3')
    attrib_name_4 = fields.Char(string='Attrib 4 Name')
    attrib_values_4 = fields.Char(string='Attrib 4 Values')
    attrib_suffixes_4 = fields.Char(string='Code Suffixes 4')
    
    #customized fields
    features = fields.Text('Features', help="Product Features")
#     feature_ids = fields.One2many('product.features','product_template_import_stage_id','Features')
    benefits = fields.Text('Benefits', help='Product benefits')
    carton_qty = fields.Float('Carton Qty', help="Carton Qty")
    pack_qty = fields.Float('Pack Qty', help="Pack Qty")
    is_clearence = fields.Boolean('Product Clearence', help="Product Clearence")
    is_website_use = fields.Boolean('Is Website Use', help='This field is used by "Convergence"')

    def attrib_to_dictionary(self, attrib_name, attrib_values):
        attribute = self.env['product.attribute'].search([('name', '=', attrib_name)])
        if not attribute:
            attribute = self.env['product.attribute'].create({'name': attrib_name})

        value_ids = []
        values = [x.strip() for x in attrib_values and attrib_values.split(';') or []]
        for value in values:
            if value:
                attribute_value = self.env['product.attribute.value'].search([('attribute_id', '=', attribute.id), ('name', '=', value)])
                if not attribute_value:
                    # WARNING!!!!!!!!!!!!!!!!!
                    # Must remove active_id in context or "create" assumes the active_id is a product_template_id!!!!!
#                     attribute_value = self.env['product.attribute.value'].with_context(remove_active_id = False).create({'attribute_id': attribute.id,
#                                                                                                                          'name': value,
#                                                                                                                          })

                    new_context = self.env.context.copy()
                    new_context.pop('active_id', None)
                    new_env = self.env(self.env.cr, self.env.uid, new_context)
                    attribute_value = self.env['product.attribute.value'].with_env(new_env).create({'attribute_id': attribute.id,
                                                                                                    'name': value,
                                                                                                    })
                value_ids.append((4, attribute_value.id))

        return {'attribute_id': attribute.id,
                'value_ids': value_ids,
                }


    def find_attribute_suffix(self, attribute_value, attrib_values, attrib_suffixes):
        return [x.strip() for x in attrib_suffixes and attrib_suffixes.split(';') or []][[x.strip() for x in attrib_values and attrib_values.split(';') or []].index(attribute_value)]

    def attrib_parses(self, attrib_name, attrib_values, attrib_suffixes):
        if len(self.env['product.attribute'].search([('name', '=', attrib_name)])) > 1:
            return 'Multiple Attribute Name found'

        values = [x.strip() for x in attrib_values and attrib_values.split(';') or []]
        suffixes = [x.strip() for x in attrib_suffixes and attrib_suffixes.split(';') or []]
        if not values:
            return 'No values'
        for value in values:
            if value:
                index = values.index(value)
                if len(suffixes) < index+1 or not suffixes[index]:
                    return 'Missing suffixes'
                if values.count(value) > 1 or suffixes.count(suffixes[index]) > 1:
                    return 'Duplicate attributes / suffixes'
        return False

    def create_lots(self,stage,new_template):
        #config_id = self.env['stock.config.settings'].search([], limit=1, order='id DESC')
        if stage.lots and new_template and self.env['res.users'].has_group('stock.group_production_lot'):
            lot_numbers_str = stage.lots.split(",")
            for lot in lot_numbers_str:
                if not self.env['stock.production.lot'].search([('name','=',lot)]):
                    self.env['stock.production.lot'].create({
                                                             'name': lot,
                                                             'product_id': new_template.product_variant_ids[0].id
                                                             })

    def create_or_update_templates(self,
                                   dry_run=True,
                                   ):
        """Create or update product templates"""
        counter = 0
        for stage in self.filtered(lambda s: not s.processed):
            counter += 1
            #Cant search and update by reference - Reference provided may only be part of the reference for multiple product.product records...
#             product = self.env["product.product"].search([''])

            stage.write({'problem': False,
                         })

#             if not stage.internal_ref:
#                 stage.write({'problem': 'No product reference'})
#                 continue

            product_data = {'name': stage.name,
                            'description': stage.description,
                            'sale_ok': stage.sale_ok,
                            'purchase_ok': stage.purchase_ok,
                            'type': stage.product_type or 'product',
                            'weight': stage.weight,
                            'volume': stage.volume,
                            'sale_delay': stage.sale_delay,
                            'warranty': stage.warranty,
                            'list_price': stage.list_price,
                            'standard_price': stage.standard_price,
                            
                            #custom fields
                            'features':stage.features,
                            'benefits':stage.benefits,
                            'carton_qty':stage.carton_qty,
                            'pack_qty':stage.pack_qty,
                            'is_clearence':stage.is_clearence,
                            'is_website_use':stage.is_website_use,
                            }
            
            if stage.tracking:
                #config_id = self.env['stock.config.settings'].search([], limit=1, order='id DESC')
                if self.env['res.users'].has_group('stock.group_production_lot'):
                    product_data['tracking'] = stage.tracking 
                else:
                    stage.write({'problem': 'The user importing the record does not have access to "Lot\Serial Number Tracking". Please provide correct access rights by navigating to "Settings -> Users".'})
                    continue
                
            if stage.invoice_policy:
                product_data['invoice_policy'] = stage.invoice_policy
            if stage.supplier:
                supplier = self.env['res.partner'].search([('name', '=', stage.supplier)])
                if not supplier:
                    stage.write({'problem': 'Supplier %s not found' % stage.supplier})
                    continue
                if len(supplier) > 1:
                    stage.write({'problem': 'Multiple Suppliers %s found' % stage.supplier})
                    continue
                seller_data = {'name': supplier.id,
                               'min_qty': stage.supplier_min,
                               'delay': stage.supplier_del,
                               'product_name': stage.supplier_product_name,
                               'product_code': stage.supplier_product_code,
                               'price': stage.supplier_price,
                               }
                if stage.supplier_currency:
                    currency = self.env['res.currency'].search([('name', '=', stage.supplier_currency)], limit=1)
                    if not currency:
                        stage.write({'problem': 'Currency %s not found' % stage.supplier_currency})
                        continue
                    seller_data['currency_id'] = currency.id
                product_data['seller_ids'] = [(0, 0, seller_data)]
            if stage.category:
                categ = self.env['product.category'].search([('name', '=', stage.category), ('type', '=', 'normal')])
                if not categ:
                    stage.write({'problem': 'Category %s not found' % stage.category})
                    continue
                if len(categ) > 1:
                    stage.write({'problem': 'Multiple Categories %s found' % stage.category})
                    continue
                product_data['categ_id'] = categ.id
            if stage.uom:
                uom = self.env['product.uom'].search([('name', '=', stage.uom)])
                if not uom:
                    stage.write({'problem': 'Unit of Measure %s not found' % stage.uom})
                    continue
                if len(uom) > 1:
                    stage.write({'problem': 'Multiple Unit of Measure %s found' % stage.uom})
                    continue
                product_data['uom_id'] = uom.id
                uom_categ_id = uom.category_id.id
            if stage.purchase_uom:
                uom = self.env['product.uom'].search([('name', '=', stage.purchase_uom)])
                if not uom:
                    stage.write({'problem': 'Unit of Measure %s not found' % stage.purchase_uom})
                    continue
                if len(uom) > 1:
                    stage.write({'problem': 'Multiple Unit of Measure %s found' % stage.purchase_uom})
                    continue
                if stage.uom and uom.category_id.id != uom_categ_id:
                    stage.write({'problem': 'Unit of Measure and Purchase Unit of Measure Category Mismatch'})
                    continue
                product_data['uom_po_id'] = uom.id
            if stage.uom and not(stage.purchase_uom):
                product_data['uom_po_id'] = product_data['uom_id']
            elif not(stage.uom) and stage.purchase_uom:
                product_data['uom_id'] = product_data['uom_po_id']
            if stage.sale_uom:
                uom = self.env['product.uom'].search([('name', '=', stage.sale_uom)])
                if not uom:
                    stage.write({'problem': 'Unit of Measure %s not found' % stage.sale_uom})
                    continue
                if len(uom) > 1:
                    stage.write({'problem': 'Multiple Unit of Measure %s found' % stage.sale_uom})
                    continue
                product_data['uos_id'] = uom.id

            if stage.company and stage.company not in ('all','All'):
                company_search = self.env['res.company'].search([('name','=',stage.company)])

                if company_search:
                    product_data['company_id'] = company_search.id
                else:
                    stage.write({'problem': 'Company %s does not exist' % stage.company})
                    continue
            elif stage.company in ('all','All'):
                product_data['company_id'] = False

            if stage.lots:
                #config_id = self.env['stock.config.settings'].search([], limit=1, order='id DESC')
                if self.env['res.users'].has_group('stock.group_production_lot'):
                    lot_numbers_str = stage.lots.split(",")
                    existing_lots = []
                    for lot in lot_numbers_str:
                        lot_search = self.env['stock.production.lot'].search([('name','=',lot)])
                        if lot_search:
                            existing_lots.append(lot_search.name)
                    if existing_lots:
                        stage.write({'problem': 'Lot(s) with the name(s) "%s" already exist. Please give different names if new lots are to be created.' % ",".join(existing_lots)})
                        continue
                else:
                    stage.write({'problem': 'The user importing the record does not have access to "Lot\Serial Number Tracking". Please provide correct access rights by navigating to "Settings -> Users".'})
                    continue

            if stage.customer_tax:
                taxes = stage.customer_tax.split(",")
                tax_ids = []
                multiple_ids = []
                notfound_ids = []
                for tax in taxes:
                    tax_id = self.env['account.tax'].search([('type_tax_use', 'in', ('sale','all')), ('name', '=', tax)])
                    if not tax_id:
                        notfound_ids.append(tax)
                    if len(tax_id) > 1:
                        multiple_ids.append(tax)
                    else:
                        tax_ids.append(tax_id.id)
                if notfound_ids:
                    stage.write({'problem': 'Customer Tax(es) "%s" not found' % ', '.join(notfound_ids)})
                    continue
                if multiple_ids:
                    stage.write({'problem': 'Multiple Customer Tax(es) found for "%s"' % ', '.join(multiple_ids)})
                    continue
                product_data['taxes_id'] = [(6, 0, tax_ids)]

            if stage.supplier_tax:
                taxes = stage.supplier_tax.split(",")
                tax_ids = []
                multiple_ids = []
                notfound_ids = []
                for tax in taxes:
                    tax_id = self.env['account.tax'].search([('type_tax_use', 'in', ('purchase','all')), ('name', '=', tax)])
                    if not tax_id:
                        notfound_ids.append(tax)
                    if len(tax_id) > 1:
                        multiple_ids.append(tax)
                    else:
                        tax_ids.append(tax_id.id)
                if notfound_ids:
                    stage.write({'problem': 'Supplier Tax(es) "%s" not found' % ', '.join(notfound_ids)})
                    continue
                if multiple_ids:
                    stage.write({'problem': 'Multiple Supplier Tax(es) found for "%s"' % ', '.join(multiple_ids)})
                    continue
                product_data['supplier_taxes_id'] = [(6, 0, tax_ids)]

            if not (stage.attrib_name_1 or stage.attrib_name_2 or stage.attrib_name_3 or stage.attrib_name_4):
                product_data.update({'default_code': stage.internal_ref,
                                     'barcode': stage.barcode,
                                     'attribute_line_ids': False,
                                     })
                if not dry_run:
                    new_template = self.env['product.template'].create(product_data)

            else:
                if stage.attrib_name_1:
                    error_str = self.attrib_parses(stage.attrib_name_1, stage.attrib_values_1, stage.attrib_suffixes_1)
                    if error_str:
                        stage.write({'problem': '%s Attribute 1 values and suffixes problem' % error_str})
                        continue
                if stage.attrib_name_2:
                    error_str = self.attrib_parses(stage.attrib_name_2, stage.attrib_values_2, stage.attrib_suffixes_2)
                    if error_str:
                        stage.write({'problem': '%s Attribute 2 values and suffixes problem' % error_str})
                        continue
                if stage.attrib_name_3:
                    error_str = self.attrib_parses(stage.attrib_name_3, stage.attrib_values_3, stage.attrib_suffixes_3)
                    if error_str:
                        stage.write({'problem': '%s Attribute 3 values and suffixes problem' % error_str})
                        continue
                if stage.attrib_name_4:
                    error_str = self.attrib_parses(stage.attrib_name_4, stage.attrib_values_4, stage.attrib_suffixes_4)
                    if error_str:
                        stage.write({'problem': '%s Attribute 4 values and suffixes problem' % error_str})
                        continue

                attribute_line_ids = []
                if stage.attrib_name_1:
                    attribute_line_ids.append((0, 0, self.attrib_to_dictionary(stage.attrib_name_1, stage.attrib_values_1)))
                if stage.attrib_name_2:
                    attribute_line_ids.append((0, 0, self.attrib_to_dictionary(stage.attrib_name_2, stage.attrib_values_2)))
                if stage.attrib_name_3:
                    attribute_line_ids.append((0, 0, self.attrib_to_dictionary(stage.attrib_name_3, stage.attrib_values_3)))
                if stage.attrib_name_4:
                    attribute_line_ids.append((0, 0, self.attrib_to_dictionary(stage.attrib_name_4, stage.attrib_values_4)))

                product_data.update({'attribute_line_ids': attribute_line_ids})

                if not dry_run:
                    new_template = self.env['product.template'].create(product_data)

                    for product in new_template.product_variant_ids:
                        suffix_1 = suffix_2 = suffix_3 = suffix_4 = ''
                        for attribute_value in product.attribute_value_ids:
                            if attribute_value.attribute_id.name == stage.attrib_name_1:
                                suffix_1 = self.find_attribute_suffix(attribute_value.name, stage.attrib_values_1, stage.attrib_suffixes_1)
                            if attribute_value.attribute_id.name == stage.attrib_name_2:
                                suffix_2 = self.find_attribute_suffix(attribute_value.name, stage.attrib_values_2, stage.attrib_suffixes_2)
                            if attribute_value.attribute_id.name == stage.attrib_name_3:
                                suffix_3 = self.find_attribute_suffix(attribute_value.name, stage.attrib_values_3, stage.attrib_suffixes_3)
                            if attribute_value.attribute_id.name == stage.attrib_name_4:
                                suffix_4 = self.find_attribute_suffix(attribute_value.name, stage.attrib_values_4, stage.attrib_suffixes_4)
                        variant_data = {'default_code': '%s%s%s%s%s' % (stage.internal_ref,
                                                                        suffix_1,
                                                                        suffix_2,
                                                                        suffix_3,
                                                                        suffix_4,
                                                                        )}
                        product.write(variant_data)
            
            if not dry_run:
                self.create_lots(stage,new_template)

                for product in new_template.product_variant_ids:
                    if stage.reorder_min or stage.reorder_max or stage.reorder_mult:
                        product.write({'orderpoint_ids': [(0, 0, {'product_min_qty': stage.reorder_min,
                                                                  'product_max_qty': stage.reorder_max,
                                                                  'qty_multiple': stage.reorder_mult,
                                                                  })]
                                       })

                stage.write({'processed': True,
                             'created_product_ids': new_template.product_variant_ids.ids,
                             })

            if counter % 100 == 0:
                self.env.cr.commit()