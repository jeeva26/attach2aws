
{
    'name': 'Mass Editing',
    'version': '10.0.1.0.0',
    'author': 'Pragmatic Techsoft Pvt. Ltd.',
    'category': 'Tools',
    'website': 'www.pragtech.co.in',
    'summary': 'Mass Editing',
    'uninstall_hook': 'uninstall_hook',
    'depends': ['base'],
    'data': [
        'security/ir.model.access.csv',
        'views/mass_editing_view.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
