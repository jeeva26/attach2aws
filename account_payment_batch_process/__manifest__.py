# -*- coding: utf-8 -*-

{
    "name": "Batch Payments Processing",
    "summary": "Process Payments in Batch",
    "version": "10.0.1",
    "author": "Pragmatic Techsoft Pvt. Ltd.",
    "category": "Generic Modules/Payment",
    "website": "www.pragtech.co.in",
    "depends": [
        "account",
        "account_check_printing",
    ],
    "data": [
        "wizard/invoice_batch_process_view.xml",
        "views/invoice_view.xml"
    ],
    "installable": True,
}
