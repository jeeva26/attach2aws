# -*- coding: utf-8 -*-
# Copyright 2010-2013 Elico Corp. <lin.yu@elico-corp.com>
# Copyright 2017 Eficent Business and IT Consulting Services S.L.
#   (http://www.eficent.com)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    'name': 'Saasmate Sale Order Line',
    'version': '10.0.1.0.0',
    'category': 'Sale',
    'summary': 'Show Sale order line details',
    'author': "MosesI",
    'website': 'www.ifensys.com',
    'license': 'AGPL-3',
    'depends': [
        'sale',
    ],
    'data': [
        'views/sale_order_line_view.xml',
        'views/menu.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
