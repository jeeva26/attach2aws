# -*- coding: utf-8 -*-

{
    'name': 'Partner Credit Limit',
    'version': '10.0.1.0.0',
    'category': 'Partner',
    'depends': ['account', 'sale'],
    'author': 'Pragmatic Techsoft Pvt. Ltd.',
    'description': '''Partner Credit Limit'
        Checks for all over due payment and already paid amount
        if the difference is positive and acceptable then Salesman
        able to confirm SO
    ''',
    'website': 'www.pragtech.co.in',
    'data': [
        'views/partner_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}
