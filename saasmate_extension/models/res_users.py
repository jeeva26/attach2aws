from odoo.tools.translate import _
from odoo import api,fields, models
class Users(models.Model):
    _name = "res.users"
    _description = 'Users'
    _inherit = 'res.users'

    @api.model
    def create(self, vals):
        user = super(Users, self).create(vals)
        if user.login:
            user.partner_id.write({'email':user.login})
        return user  
    
Users()
