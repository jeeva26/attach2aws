# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError, except_orm, UserError
import logging
_logger = logging.getLogger(__name__)
from odoo.exceptions import Warning
from odoo.tools.translate import _

from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

from cStringIO import StringIO
from odoo.tools.misc import xlwt
import base64

class saleOrderLinexls(models.TransientModel):
    _name = 'sale.order.line.xls'
    _description ='Export sale order in export(xls)'

    data = fields.Binary('File', readonly=True)
    name = fields.Char('Filename', size=256, readonly=True)
    
    def export_to_excel(self):
        """Export quotation lines in excel"""
        workbook = xlwt.Workbook(encoding ='utf-8')
        worksheet = workbook.add_sheet('Sale Order Lines')
        sale_line_fields = ['Product','Section','Description','Ordered Qty','Delivered','Invoiced','Unit of Measure','Analytic Tags','Route','Unit Price','Cost','Margin','Taxes','Discount','Subtotal','Total']
        header_style = xlwt.easyxf('pattern: pattern solid, fore_colour green;') 
        for i, fieldname in enumerate(sale_line_fields):
            worksheet.write(0, i, fieldname,header_style)
            worksheet.col(i).width = 8000 # around 220 pixels
 
        base_style = xlwt.easyxf('align: wrap yes')
        date_style = xlwt.easyxf('align: wrap yes', num_format_str='YYYY-MM-DD')
        datetime_style = xlwt.easyxf('align: wrap yes', num_format_str='YYYY-MM-DD HH:mm:SS')
        
        order_ids = self._context.get('active_ids')
        orders = self.env['sale.order'].browse(order_ids)
        
        for order in orders:
            line_cnt = 1
            
            for line in order.order_line:
                worksheet.write(line_cnt, 0, line.product_id.name, base_style)
                worksheet.write(line_cnt, 1, line.layout_category_id.name, base_style)
                worksheet.write(line_cnt, 2, line.name, base_style)
                worksheet.write(line_cnt, 3, line.product_uom_qty, base_style)
                worksheet.write(line_cnt, 4, line.qty_delivered, base_style)
                worksheet.write(line_cnt, 5, line.qty_invoiced, base_style)
                worksheet.write(line_cnt, 6, line.product_uom.name, base_style)
                analytic_tag_list = [str(analytic.name) for analytic in line.analytic_tag_ids ]
                analytic_tags = ','.join(analytic_tag_list)
                worksheet.write(line_cnt, 7, analytic_tags, base_style)
                worksheet.write(line_cnt, 8, line.route_id.name, base_style)
                worksheet.write(line_cnt, 9, line.price_unit, base_style)
                worksheet.write(line_cnt, 10, line.purchase_price, base_style)
                worksheet.write(line_cnt, 11, line.margin, base_style)
                taxes_list = [tax.name for tax in line.tax_id]
                taxes = ','.join(taxes_list)
                worksheet.write(line_cnt, 12, taxes, base_style)
                worksheet.write(line_cnt, 13, line.discount, base_style)
                worksheet.write(line_cnt, 14, line.price_subtotal, base_style)
                worksheet.write(line_cnt, 15, line.price_total, base_style)
                line_cnt += 1
 
        fp = StringIO()
        workbook.save(fp)
        out = base64.encodestring(fp.getvalue())
        abc = self.create({'data': out,'name': 'sale_order_line.xls'})
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'sale.order.line.xls',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': abc.id,
            'views': [(False, 'form')],
            'target': 'new',
        }
saleOrderLinexls()

class SaleOrder(models.Model):
    _inherit = "sale.order"
    
    @api.model
    def _get_date(self):
        return (datetime.today() + timedelta(days=30)).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
    
    @api.model
    def _default_warehouse_id(self):
        '''Do not set default warehouse Id. Should be selected from order line product's division'''
        return
    
    '''partner_farmer_bdm is related fields to customers taken for group by in sale order'''
    partner_farmer_bdm = fields.Many2one(related='partner_id.classification_id', store="True")
    
    partner_sales_executive = fields.Many2one('res.users', related='partner_id.sales_executive', store="True", string = "Sales Executive") #used to print in report
    client_order_ref = fields.Char(string='Customer Reference')
#     report_template_id = fields.Many2one('report.templates','Template')
    with_without_image = fields.Selection([('with_image','With Image'),('without_image','Without Image')],'With/Without Image')
    validity_date = fields.Date(string='Expiration Date', readonly=True, copy=False, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},default= _get_date,
        help="Manually set the expiration date of your quotation (offer), or it will set the date automatically based on the template if online quotation is installed.")
#     warehouse_id = fields.Many2one(
#         'stock.warehouse', string='Warehouse',
#         required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
#         default=_default_warehouse_id)
    
    warehouse_id = fields.Many2one(
        'stock.warehouse', string='Warehouse',
        required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    
    @api.onchange('client_order_ref')
    def _onchange_client_order_ref(self):
        if self.client_order_ref:
            flag = False
            for order in self.env['sale.order'].search([]):
#                 re.search(r'\b' + vals['client_order_ref'] + '\W', order.client_order_ref)
                if order.client_order_ref and self.client_order_ref.find(order.client_order_ref) > -1:
                    flag = True
                    break
            if flag:
                return {'warning':{'title': _('Warning!'), 'message': _('Client order ref already exist')}}
        return {}

    @api.multi
    def action_confirm(self):
        if not self.client_order_ref:
            raise Warning(_('Please enter Customer Reference!'))
        return super(SaleOrder,self).action_confirm()

SaleOrder()
    
class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    @api.multi
    def create_invoices(self):
        '''Setting same report template as for SO in invoices on create of invoice'''
        res = super(SaleAdvancePaymentInv,self).create_invoices()
#         sale_orders = self.env['sale.order'].browse(self._context.get('active_ids', []))
#         for each_sale in sale_orders:
#             if each_sale.report_template_id:
#                 inv_ids = [each_inv.id for each_inv in each_sale.invoice_ids]
#                 new_inv = each_sale.env['account.invoice'].search([('id','in',inv_ids),('report_template_id','=',False)])
#                 new_inv.write({'report_template_id':each_sale.report_template_id.id})
        return res
