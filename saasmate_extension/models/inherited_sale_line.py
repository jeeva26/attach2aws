# -*- coding: utf-8 -*-
from odoo import api, fields, models
import odoo.addons.decimal_precision as dp


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    production_dashboard_ids = fields.One2many('production.dashboard.line','sale_order_id','Production Dashboard')
    
    @api.onchange('order_line')
    def onchange_product(self):
        product_list = [];
        for record in self.order_line:
            product_list.append((0,0,{'requested_date':self.date_order,'partner_id':self.partner_id.id, 'product_id':record.product_id.id}))
        if product_list:
            self.production_dashboard_ids = product_list
    

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    margin_percentage = fields.Float(compute='_product_margin', digits=dp.get_precision('Product Price'), store=True,string='Margin(%)')
    requested_date = fields.Date('Requested Date') 
    expected_date = fields.Date('Expected Date')
    
    @api.depends('product_id', 'purchase_price', 'product_uom_qty', 'price_unit', 'price_subtotal')
    def _product_margin(self):
        for line in self:
            currency = line.order_id.pricelist_id.currency_id
            line.margin = currency.round(line.price_subtotal - ((line.purchase_price or line.product_id.standard_price) * line.product_uom_qty))
            #Added By susai
            if line.price_unit != 0:
                line.margin_percentage = currency.round((line.margin / line.price_unit ) * 100)
            else:
                line.margin_percentage = 0


class ProductionDashboardLine(models.Model):
    _name = "production.dashboard.line"
    _description = 'Production Dashboard Line Details'
    _rec_name = 'partner_id'
    
    sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    requested_date = fields.Date('Requested Date') 
    partner_id = fields.Many2one('res.partner', string='Customer', requried=True)
    product_id = fields.Many2one('product.product', string='Product', domain=[('sale_ok', '=', True)], change_default=True, ondelete='restrict')
    p_p = fields.Boolean('P')
    fab = fields.Boolean('FAB')
    qc2 = fields.Boolean('QC2')
    paint = fields.Boolean('PAINT')
    qc3 = fields.Boolean('QC3')
    ship = fields.Boolean('SHIP')
    dispatched_date = fields.Date('Dispatched Date')
    comments = fields.Char('Comments')
    eta_date = fields.Date('NEW ETA Date')
    
    