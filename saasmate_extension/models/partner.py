# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import logging
from odoo import SUPERUSER_ID

_logger = logging.getLogger(__name__)

class ResPartner(models.Model):
    _inherit="res.partner"
    
    farmer_bdm = fields.Selection([('farmer','Farmer'),('bdm','BDM')])
    classification_id = fields.Many2one('res.partner.classification','Classification')
    sales_executive = fields.Many2one('res.users','Sales Executive')
    abn_number = fields.Char('ABN/IRD') #used for displaying on tax code
#     template = fields.Selection([('home_division','Home Division'),('health_division','Health Division')])
    notes = fields.Text('Notes',help="Reference notes") #reference notes specific to the customer.
#     account_journal_id = fields.Many2one('account.journal','Payment Types',domain=[('type', 'in', ['bank','cash'])],help='Select journal type') #Payment type
    account_journal_id = fields.Selection([('direct_credit','Direct Credit'),('other','Other')],string="Payment Type",help='Select journal type') #Payment type
    vendor_ref = fields.Char('Vendor Reference')
        
#     @api.multi
#     def write(self, vals):
#         res = super(ResPartner,self).write(vals)
#         for record in self:
#             children = self.env['res.partner'].search([('parent_id','=',record.id)])
#             super(ResPartner,children).write({'template':record.template,'classification_id':record.classification_id.id,'industrysector_id':record.industrysector_id.id})
#         return res
    
ResPartner()
    
class ResPartnerClassification(models.Model):
    _name="res.partner.classification"
    description="Partner classification master e.g. Farmer, BDM"
    
    name = fields.Char(string="Classification", help="Partner classification name such as BDM, Farmer etc.")
    
ResPartnerClassification()    