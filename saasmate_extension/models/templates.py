# -*- coding: utf-8 -*-

from odoo import fields, models
import logging
_logger = logging.getLogger(__name__)

class ReportTemplates(models.Model):
    _name="report.templates"
    _description="Report template master which will contain company name and logo used on different report."
    
    name = fields.Char('Name')
    logo = fields.Binary('Logo')
    