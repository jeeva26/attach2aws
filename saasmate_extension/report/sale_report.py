# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api


class SaleReport(models.Model):
    _inherit = 'sale.report' 

    sales_executive = fields.Many2one('res.users','Sales Executive')
#     farmer_bdm = fields.Selection([('farmer','Farmer'),('bdm','BDM')],'Farmer/BDM')
    classification_id = fields.Many2one('res.partner.classification', 'Classification')
    purchase_price = fields.Float('Cost Price')
    margin = fields.Float('Profit')
    margin_per = fields.Float('Profit(%)')

    
    def _select(self):
        return super(SaleReport, self)._select() + ", 0 AS margin_per, s.partner_sales_executive as sales_executive, s.partner_farmer_bdm as classification_id, sum(l.purchase_price * l.product_uom_qty) as purchase_price"

    def _group_by(self):
        return super(SaleReport, self)._group_by() + ",  s.partner_sales_executive, s.partner_farmer_bdm, l.margin"
    
    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        res = super(SaleReport, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
        if res:
            if res[0].has_key('price_subtotal') and res[0].has_key('margin_per') and res[0].has_key('margin') and res[0].has_key('purchase_price'):
                for name in res:
                    if not name['purchase_price']:
                        name['purchase_price']=0
                    if not name['price_subtotal']:
                        name['price_subtotal']=0
                        name['margin_per']=0
                    else:
                        if not name['margin']:
                            name['margin'] = 0
                        name['margin']=name['price_subtotal']-name['purchase_price']
                        name['margin_per']=(name['margin']/name['price_subtotal'])*100
        return res