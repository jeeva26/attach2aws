# -*- coding: utf-8 -*-
{
    'name': 'CRM Claim RMA Code',
    'category': 'Generic Modules/CRM & SRM',
    'author': 'Pragmatic Techsoft Pvt. ltd.',
    'website': 'www.pragtech.co.in',
    'description': """
RMA for Amtech
====================

""",    
    'depends': [
        'crm_claim_type',
        'crm_claim_code',
    ],
    'data': [
        'data/ir_sequence_type.xml',
        'views/crm_claim_type.xml',
    ],
    'installable': True,
}
