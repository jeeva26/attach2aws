# -*- coding: utf-8 -*-

{
 'name': 'RMA Claims Advance Location',
 'category': 'Generic Modules/CRM & SRM',
 'version': '1.0',   
 'depends': ['crm_claim_rma',
             'crm_rma_stock_location',
             ],
 'author': 'Pragmatic Techsoft Pvt. ltd.',
 'website': 'www.pragtech.co.in',
 'description': """
RMA Claim Advance Location
==========================

This module adds the following location on warehouses :

 * Carrier Loss
 * RMA
 * Breakage Loss
 * Refurbish
 * Mistake Loss

And also various wizards on icoming deliveries that allow you to move your goods easily in those
new locations from a done reception.

Using this module make the logistic flow of return a bit more complexe:

 * Returning product goes into RMA location with a incoming shipment
 * From the incoming shipment, forward them to another places (stock, loss,...)

WARNING: Use with caution, this module is currently not yet completely debugged and is waiting his author to be.

""",
    'images': [],
    'demo': [],
    'data': ['wizard/claim_make_picking_from_picking_view.xml',
              'wizard/claim_make_picking_view.xml',
              'stock_view.xml',
              'stock_data.xml',
              'claim_rma_view.xml',
    ],
    'installable': True,
    'application': True,
}
