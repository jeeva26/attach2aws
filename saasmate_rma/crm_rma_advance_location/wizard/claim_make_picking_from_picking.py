# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo import netsvc
import time


class claim_make_picking_from_picking(models.TransientModel):

    _name = 'claim_make_picking_from_picking.wizard'
    _description = 'Wizard to create pickings from picking lines'

    picking_line_source_location = fields.Many2one('stock.location',
            'Source Location',
            help="Location where the returned products are from.",
            required=True)
    picking_line_dest_location = fields.Many2one('stock.location',
            'Dest. Location',
            help="Location where the system will stock the returned products.",
            required=True)
    picking_line_ids = fields.Many2many('stock.move',
            'claim_picking_line_picking',
            'claim_picking_id',
            'picking_line_id',
            'Picking lines')
    
    @api.one
    def _get_default_warehouse(self):
        warehouse_id=self.env['crm.claim']._get_default_warehouse()
        return warehouse_id
    
    @api.multi
    def _get_picking_lines(self):
        return self.env['stock.picking'].read(self._context['active_id'], ['move_lines'])['move_lines']

    # Get default source location
    @api.multi
    def _get_source_loc(self):
        if self._context is None: 
            context = {}
        warehouse_obj = self.env['stock.warehouse']
        warehouse_id = self._get_default_warehouse()
        return warehouse_obj.read(warehouse_id, ['lot_rma_id'], context=context)['lot_rma_id'][0]

    # Get default destination location
    @api.one
    def _get_dest_loc(self):
        if self._context is None: 
            context = {}
        warehouse_id = self._get_default_warehouse()
        warehouse_obj = self.env['stock.warehouse']
        if self._context.get('picking_type'):
            context_loc = self._context.get('picking_type')[8:]
            loc_field = 'lot_%s_id' %context_loc
            loc_id = warehouse_obj.read(warehouse_id, [loc_field])[loc_field][0]
        return loc_id

    _defaults = {
        'picking_line_source_location': _get_source_loc,
        'picking_line_dest_location': _get_dest_loc,
        'picking_line_ids': _get_picking_lines,
    }

    @api.multi
    def action_cancel(self):
        return {'type': 'ir.actions.act_window_close',}

    # If "Create" button pressed
    @api.multi
    def action_create_picking_from_picking(self):
        picking_obj = self.env['stock.picking']
        move_obj = self.env['stock.move']
        view_obj = self.env['ir.ui.view']
        if self._context is None: 
            context = {}
        p_type = 'internal'
        if self._context.get('picking_type'):
            context_type = self._context.get('picking_type')[8:]
            note = 'Internal picking from RMA to %s' %context_type
            name = 'Internal picking to %s' %context_type
        view_id = view_obj.search([
                                            ('xml_id', '=', 'view_picking_form'),
                                            ('model', '=', 'stock.picking'),
                                            ('type', '=', 'form'),
                                            ('name', '=', 'stock.picking.form')
                                            ])[0]
        wizard = self.browse(self._ids[0])
        prev_picking = picking_obj.browse(self._context['active_id'])
        partner_id = prev_picking.partner_id.id
        # create picking
        picking_id = picking_obj.create({
                    'origin': prev_picking.origin,
                    'type': p_type,
                    'move_type': 'one', # direct
                    'state': 'draft',
                    'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                    'partner_id': prev_picking.partner_id.id,
                    'invoice_state': "none",
                    'company_id': prev_picking.company_id.id,
                    'location_id': wizard.picking_line_source_location.id,
                    'location_dest_id': wizard.picking_line_dest_location.id,
                    'note' : note,
                    'claim_id': prev_picking.claim_id.id,
                })
        # Create picking lines
        for wizard_picking_line in wizard.picking_line_ids:
            move_id = move_obj.create({
                    'name' : wizard_picking_line.product_id.product_tmpl_id.name, # Motif : crm id ? stock_picking_id ?
                    'priority': '0',
                    #'create_date':
                    'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                    'date_expected': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                    'product_id': wizard_picking_line.product_id.id,
                    'product_qty': wizard_picking_line.product_qty,
                    'product_uom': wizard_picking_line.product_uom.id,
                    'partner_id': prev_picking.partner_id.id,
                    'prodlot_id': wizard_picking_line.prodlot_id.id,
                    # 'tracking_id':
                    'picking_id': picking_id,
                    'state': 'draft',
                    'price_unit': wizard_picking_line.price_unit,
                    # 'price_currency_id': claim_id.company_id.currency_id.id, # from invoice ???
                    'company_id': prev_picking.company_id.id,
                    'location_id': wizard.picking_line_source_location.id,
                    'location_dest_id': wizard.picking_line_dest_location.id,
                    'note': note,
                })
            wizard_move = move_obj.write(
                wizard_picking_line.id,
                {'move_dest_id': move_id})
        wf_service = netsvc.LocalService("workflow")
        if picking_id:
            wf_service.trg_validate(self._uid,'stock.picking', picking_id,'button_confirm', self._cr)
            picking_obj.action_assign(self._cr, self._uid, [picking_id])
        domain = "[('type','=','%s'),('partner_id','=',%s)]"%(p_type, partner_id)
        return {
            'name': '%s' % name,
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'domain' : domain,
            'res_model': 'stock.picking',
            'res_id': picking_id,
            'type': 'ir.actions.act_window',
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
