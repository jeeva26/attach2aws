# -*- coding: utf-8 -*-

{
    'name': 'RMA Stock Location',
    'version': '1.0.0',
    'author': 'Pragmatic Techsoft Pvt. ltd.',
    'website': 'www.pragtech.co.in',
    'category': 'Generic Modules/CRM & SRM',
    'depends': [
        'crm_claim_rma',
        'crm_claim',
        'stock_account',
        'procurement',
        'crm_rma_location',
    ],
    'data': [
        'wizards/claim_make_picking_from_picking_view.xml',
        'wizards/claim_make_picking_view.xml',
        'views/product_product.xml',
        'views/product_template.xml',
        'views/crm_claim.xml',
        'views/stock_picking.xml',
        'views/stock_warehouse.xml',
    ],
    'demo': [
        'demo/stock_location.xml',
        'demo/stock_inventory.xml',
    ],
    'post_init_hook': 'post_init_hook',
    'installable': True,
    'auto_install': False,
}
