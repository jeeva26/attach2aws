# -*- coding: utf-8 -*-
from openerp import fields, models

class ResCompany(models.Model):

    _inherit = "res.company"

    crm_return_address_id = fields.Many2one(
        'res.partner',
        string='Return address',
        help="Default address where the customers has to send back the "
             "returned product. If empty, the address is the "
             "company address")
