from odoo import api, fields, models, _
from odoo.exceptions import UserError

class AccountInvoice(models.Model):

    _inherit = "account.invoice"

    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice', 'type')
    def _compute_amount(self):
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        self.amount_tax = sum(line.amount for line in self.tax_line_ids)
        self.amount_total = self.amount_untaxed + self.amount_tax
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign    

    claim_id = fields.Many2one('crm.claim',"Claim")
    amount_tax = fields.Monetary(string='Tax',
        store=True, readonly=True, compute='_compute_amount')
#     claim_type = fields.Many2one(related="claim_id.claim_type")
    
    def _refund_cleanup_lines(self, lines):
        """ Override when from claim to update the quantity and link to the
        claim line.
        """

        # check if is an invoice_line and we are from a claim
        if not (self.env.context.get('claim_line_ids') and lines and
                lines[0]._name == 'account.invoice.line'):
            return super(AccountInvoice, self)._refund_cleanup_lines(lines)

        # start by browsing all the lines so that Odoo will correctly prefetch
        line_ids = [l[1] for l in self.env.context['claim_line_ids']]
        claim_lines = self.env['claim.line'].browse(line_ids)

        new_lines = []
        for claim_line in claim_lines:
            if not claim_line.refund_line_id:
                # For each lines replace quantity and add claim_line_id
                inv_line = claim_line.invoice_line_id
                clean_line = {}
                for field_name, field in inv_line._fields.iteritems():
                    column_type = field.type
                    if column_type == 'many2one':
                        clean_line[field_name] = inv_line[field_name].id
                    elif column_type not in ('many2many', 'one2many'):
                        clean_line[field_name] = inv_line[field_name]
                    elif field_name == 'invoice_line_tax_ids':
                        tax_ids = inv_line[field_name].ids
                        clean_line[field_name] = [(6, 0, tax_ids)]
                clean_line['quantity'] = claim_line.product_returned_quantity
                clean_line['claim_line_id'] = [claim_line.id]

                new_lines.append(clean_line)
        if not new_lines:
            # TODO use custom states to show button of this wizard or
            # not instead of raise an error

            raise UserError(
                _('A refund has already been created for this claim !')
            )
        return [(0, 0, l) for l in new_lines]

    @api.model
    def _prepare_refund(self, *args, **kwargs):
        result = super(AccountInvoice, self)._prepare_refund(*args, **kwargs)

        if self.env.context.get('claim_id'):
            result['claim_id'] = self.env.context['claim_id']

        return result
    
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        if 'claim_type_id' in self._context:
            claim_type_id = self._context.get('claim_type_id')
            cust_claim_type_id = self.env.ref('crm_claim_type.crm_claim_type_customer').id
            supp_claim_type_id = self.env.ref('crm_claim_type.crm_claim_type_supplier').id
            if view_type == 'form':
                if claim_type_id == cust_claim_type_id:
                    view_id = self.env.ref('account.invoice_form').id
                elif claim_type_id == supp_claim_type_id:
                    view_id = self.env.ref('account.invoice_supplier_form').id
            elif view_type == 'tree':
                if claim_type_id == cust_claim_type_id:
                    view_id = self.env.ref('account.invoice_tree').id
                elif claim_type_id == supp_claim_type_id:
                    view_id = self.env.ref('account.invoice_supplier_tree').id
        return super(AccountInvoice, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)  
    
    @api.model
    def create(self, vals):
        res = super(AccountInvoice, self).create(vals)
        res.compute_taxes()
        return res      

