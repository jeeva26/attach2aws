# -*- coding: utf-8 -*-

{
    'name': 'crm_claim_categ_as_name',
    'version': '1.0',
    'category': 'Generic Modules/CRM & SRM',
    'author': 'Pragmatic Techsoft Pvt. ltd.',
    'website': 'www.pragtech.co.in',
    'description':
    """
    Replace claim name by category. It makes easier to filter on claims.
    """,
    'depends': ['crm_claim_rma'],
    'data': [
        'claim_view.xml',
    ],
    'demo': [],
    'installable': True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
