# -*- coding: utf-8 -*-
{
    'name': 'Remove Company Branding',
    'version': '1.0',
    'category': 'General',
    'sequence': 3,
    'summary': 'Remove Company Branding',
    'author': 'Pragmatic Techsoft Pvt. ltd.',
    'website': 'http://pragtech.co.in',
    'description': """
Remove Company Branding
========================

Remove company branding in secondary menu. 

""",
    'depends': ['web'],
    'data': [
        'views/remove_company_logo.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
